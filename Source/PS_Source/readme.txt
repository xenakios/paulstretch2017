PaulStretch3

This is an experimental program for audio stretching and processing.
It is suitable only for extreme stretching and processing of the audio. 

Copyright (C) 2006-2011 Nasca Octavian Paul, Tg. Mures, Romania

Copyright (C) 2017 Xenakios

Released under GNU General Public License v.2 license

Source code :

https://bitbucket.org/xenakios/paulstretch2017/src

Requirements for building from source code :
    -C++17 compiler and C++ standard library
    -JUCE 5.2 : https://github.com/WeAreROLI/JUCE
    -FFTW3

History:
    20060527(0.0.1)
	  - First release

    20060530(0.0.2)
	  - Ogg Vorbis output support
	  - Added a wxWidgets graphical user interface

    20060812(1.000)
	  - Removed the wxWidgets GUI and added a FLTK GUI (because FLTK GUI is smaller)
	  - Added real-time processing/player
	  - Added input support for Ogg Vorbis files
	  - Improved the stretch algorithm and now the amount of stretch is unlimited (and on big stretch amounts, you don't need additional memory)
	  - Added "Freeze" button to the player
	  - It is possible to render to file only a selected part of the sound
	  - Other improvements    

    20060905(1.024)
	  - Added MP3 support for input
	  - Added bypass mode (if you click play with the right mouse button)
	  - Improved the precision of the position slider (now it shows really what's currenly playing)
	  - Added the possibility to set the stretch amount by entering the numeric value
	  - Added pause mode and volume control
	  - Added post-processing of the spectrum(pitch/frequency shift, octave mixer, compress,filter,harmonics)
	  - Command line parameter for input filename 
	  - The file can be dragged from the explorer to the file text to open it

    20090424(2.0)
	  - Added free envelopes, which allows the user to freely edit some parameters
	  - Added stretch multiplier (with free envelope) which make the stretching variable
	  - Added arbitrary frequency filter
	  - Added a frequency spreader effect, which increase the bandwith of each harmonic
	  - Added a frequency shifter which produces binaural beats (the beats frequencies are variable)
	  - Added 32 bit WAV rendering
	  - Other improvements and bugfixes
	
    20110210(2.1)
	  - Added loading/saving parameters
	  - Added Linux Jack support (thanks to Robin Gareus for the patch)
	  - Added "Symmetric" mode of Binaural Beats
	  - Support for longer stretches - for the really patient ones - up to one quintillion times  ( 10^18 x ) ;-)
	  - Fixed a bug which produced infinite loop at the end of some mp3 files (at playing or render)
	  - Fixed a bug in the mp3 reader
	  - other minor additions

    20110211(2.1-0)
          - Increased the precision of a paremeter for extreme long stretches
    
    20110303(2.2)
          - Improved the stretching algorithm, adding the onset detection
          - Shorten algorithm improvements 
          - Added an option to preserve the tonal part or noise part
	  - Ignored the commandline parameters starting with "-" (usefull for macosx)

    20110305(2.2-0)
          - gzip bugfix which prevents loading of the .psx files on Windows 
          - bugfix on freeze function on onsets

    20110305(2.2-1)
	  - removed the noise on starting/seeking with the player
          - bugfix on freeze function 

    20110306(2.2-2)
	  - buffer error on render

	2017-09-14(3.0.0beta1, Xenakios fork)
	  - GUI and audio IO (sound files, audio hardware) code replaced with JUCE based code.
	  - Not all of the existing features are yet available from the GUI, but hopefully eventually they will be.
      - Audio file reading code changed to use floating point samples to get 
	    the benefits of the extended dynamic range of floating point files.
	  - Resample audio to audio interface samplerate, if needed
	  - Allow choosing prebuffering amount. Smaller amounts allow faster response to parameter 
	    changes while realtime playing
	  - Efforts to modernize the code base to use smart pointers, std::vector for audio buffers etc.
	  
	2017-09-14 (3.0.0beta2, Xenakios fork)
		-Added wav file format options for rendering
		-Added some keyboard shortcuts : Space bar for play/stop, 'F' for toggle freeze, Home for seek to beginning 
		-Allow drag and drop of audio file into app window
		-Menu button menu shows recently used files
		-Audio in realtime playback is clipped after volume adjustment
	
	2017-09-14 (3.0.0beta3, Xenakios fork)
		-Fixed buffer size bug in offline rendering code
		-Prevent rendering over the input file
		-Added audio output visualizer (can hide if takes too much CPU)
		-Added feature to allow recording live audio to disk
		-Disable FFT size slider while playing back. (Need to be determined later if the FFT size can be changed in real time.)
	
	2017-09-15 (3.0.0beta4, Xenakios fork)
		-When audio stopped, refresh the GUI components less often to reduce CPU consumption
		-If opening audio output device fails, show error message on application start-up
		-Preliminary audio looping support (does not crossfade yet, so may cause additional noise)
	
	2017-09-19 (3.0.0beta5, Xenakios fork)
		-New playback code that is shared for realtime and offline rendering use.
		-Multichannel input files, audio output device and rendering support.
		-Allow toggling looping playback.
		-Most processing parameters now exposed in the GUI.
		-Delete previously existing file when rendering.
		-Binary built with FFTW3 library. Allow FFTW3 to optionally do its plans slowly to 
		 possibly result in faster FFTs. (May take several seconds before playback will start, so mostly an
		 experimental feature, not recommended for general use.)
		-Use Mersenne Twister random generator for the spectral phases.
		-CPU and prebuffering ready meters. (Note that on faster machines the CPU meter may never show any load,
		 as it measures the time spent in the realtime audio callback.)
	2017-09-26 (3.0.0beta6, Xenakios fork)
		-Spectral processing stages can be reordered (all reorderings won't sound very different, but some will).
		-Added smoothing to main output volume processing.
		-Loop playback crossfade.
		-Fixes and optimizations.
		-MacOs version now requires OS-X 10.9 at minimum. 
	2017-09-26 (3.0.0beta7, Xenakios fork)
		-Rebuilt FFTW for Windows with only SSE2 instructions required.
		-New prebuffering option Huge added.
		-If above enabled, the FFT processing size can go up to around 60 seconds.
		-Shortcut key 0 restores default parameter values except main volume. (FFT size still won't update while playing back, though.)
		-Logic to detect appropriate playback/render stop point improved
	2017-10-13 (3.0.0preview8, Xenakios fork)	
		-Allow changing FFT size while playing back. (Setting is applied when mouse button released from slider, 
		 not while moving the mouse due to complicated technical reasons.)
		-Changing input file while playing back crossfades to the new file.
		-Show a more correct output duration in the GUI. (With huge FFT sizes and a short input file, the FFT size 
		 can affect the final output duration much more than the stretch factor.)
		-Allow up to 32 output channels.
		-Import support for Wavpack, Monkey's Audio, Musepack, Shorten, TAK, MP4, MKV, AVI and various other files by 
		 converting them into WAV with ffmpeg.
		-Settings window instead of the popup menu.
		-Added ability to choose FFT windowing type. Just leaving it as Hamming is recommended, though.
		-Added some tooltips to parameters. (Shown when hovering over the parameter name.)
	2017-10-30 (3.0.0preview9, Xenakios fork)
		-MIDI CC learn support by right-click on parameter slider/button/dropdown. (FFT size parameter not yet supported.) 
		 Also allows changing the mapping curve from MIDI CC value to parameter value.
		-Added saving and loading of paulstretch3 projects.
		-Added zoom/scrollbar to waveform component
		-Main volume applied when offline rendering.
		-Added option to enable/disable clipping for 32 bit floating point renders (16 and 24 bit outputs are always clipped).
		-Can change harmonics base frequency parameter to Herz based by right-clicking the slider.
		-Changed GUI layout so that the parameters of the different spectral processing modules 
		 are inside group components. (Needs tweaking...)
		-Format output duration as minutes, hours etc as needed
	2017-11-10 (3.0.0preview10, Xenakios fork)
		-Allow stretch amounts lower than 1.0 for shortening
		-Optimizations to avoid unnecessary deallocations/reallocations of buffers and objects
		-Rendering now opens a window that has rendering options, including an approximate loop count
		 setting when looping has been enabled.
		-Added shortcut keys for waveform zooming
		-Waveform time selection edges can be dragged to adjust the selection
		-Project files can be passed into command line argument
		-Project files can be drag and dropped into application window
		-MIDI note based harmonics generator base frequency calculation fix and allow setting tuning from slider 
		 right-click menu
		-Prevent importing 64 bit floating point files as those are not supported by JUCE
	2017-11-17 (3.0.0.preview11, Xenakios fork)
		-Fix bug with multichannel input files in shortening mode
	2018-01-11 (3.0.0.preview12, Xenakios fork)
		-Increase maximum stretch amount to 1024x.
		
Keyboard shortcuts (as of version 3.0.0 preview10) :

Space bar : Start/Stop playback
F : Toggle freeze
0 : Reset parameters to default values
Control/Command+S : Save current project as
Control/Command+O : Open project file
Control/Command++ : Zoom to waveform time selection
Control/Command+- : Zoom to show whole waveform
Home : Seek playback to beginning of file
		
Enjoy! :)
Paul and Xenakios

zynaddsubfx_AT_yahoo com
xenakios_AT_gmail com
