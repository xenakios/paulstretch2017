/*
Copyright (C) 2006-2011 Nasca Octavian Paul
Author: Nasca Octavian Paul

Copyright (C) 2017 Xenakios

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License (version 2) for more details.

You should have received a copy of the GNU General Public License (version 2)
along with this program; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "PS_Source/PaulStretchControl.h"
#include "MyComponents.h"
#include <functional>
#include <map>
#include "Lua/sol.hpp"
#include "envelope_component.h"

enum class RemoteSource { MIDI_CC, MIDI_Note, OSC };
enum class RemoteDestination { Parameter, Button };

class RemoteMapping
{
public:
	RemoteMapping(RemoteSource rtype, int midichan, int ccnum, RemoteDestination desttype, int dest) :
		m_src_type(rtype), m_midicc(ccnum), m_midichan(midichan), m_dest_type(desttype), m_dest(dest) 
	{
		m_map_env = std::make_shared<breakpoint_envelope>();
		m_map_env->AddNode({ 0.0,0.0 });
		m_map_env->AddNode({ 1.0,1.0 });
		m_map_env->set_reset_nodes(m_map_env->get_all_nodes());
	}
	RemoteSource m_src_type;
	int m_midicc = -1;
	int m_midichan = -1;
	int m_dest = -1;
	RemoteDestination m_dest_type = RemoteDestination::Parameter;
	std::shared_ptr<breakpoint_envelope> m_map_env;
};

class MainContentComponent   : public Component, 
	public Button::Listener, 
	public Slider::Listener,
	public MultiTimer,
    public FileDragAndDropTarget, public DragAndDropContainer,
	public MidiInputCallback

{
public:
    //==============================================================================
    MainContentComponent();
    ~MainContentComponent();
    void buttonClicked(Button* but) override;
    void paint (Graphics&) override;
    void resized() override;
	void sliderValueChanged(Slider* slid) override;
	void setAudioFile(File file);
	void timerCallback(int id) override;
    bool keyPressed(const KeyPress& ev) override;
    bool isInterestedInFileDrag (const StringArray &files) override;
    void filesDropped (const StringArray &files, int x, int y) override;
    void startPlay();
    void stopPlay();
    
	void handleIncomingMidiMessage(MidiInput *source, const MidiMessage &message) override;
	void saveDocument();
	void saveDocumentAs();
	void openDocument();
	void loadLuaMIDIScript(File f);
	void loadLuaGUIScript(File f);
	Control* getPSControl() { return m_thecontrol.get(); }
	void renderToFile(File f, int64_t numLoops, int wavformat, int sampleRate, bool clipfloats, double maxdur);
	void showRenderWindow();
	String loadStateFromFile(File f);
private:
    std::unique_ptr<Control> m_thecontrol;
    TextButton m_importfilebut;
	TextButton m_recentfilesbut;
	TextButton m_playbut;
	MyButton m_rewindbut;
	TextButton m_renderbut;
    TextButton m_menubut;
	TextButton m_move_spec_up;
	TextButton m_move_spec_down;
	
	
	GroupComponent m_group_stretcher;
	GroupComponent m_group_pitch;
	GroupComponent m_group_harmonics;
	GroupComponent m_group_tonalvsnoise;
	GroupComponent m_group_filter;
	GroupComponent m_group_compressor;
	GroupComponent m_group_spread;
	GroupComponent m_group_freqshift;
	GroupComponent m_group_specorder;
	SpectralProcessesListBox m_spec_listbox;
	double m_render_progress = 0.0;
	ProgressBar m_progressbar;
	std::vector<std::shared_ptr<ParamComponent>> m_param_components;
	Label m_stretchinfolabel;
	WaveformComponent m_wavecomponent;
	AudioVisualiserComponent m_aviscomp;
    PerfMeterComponent m_perfcomponent;
	void updateInfoLabel();
	File m_curfile;
	File m_liverecordfile;
	RenderInfoRef m_renderinfo;
	void showAboutScreen();
	void showMiscMenu();
	void updateStretchParamsFromSliders();
    
    RecentlyOpenedFilesList m_recent_audio_files;
	RecentlyOpenedFilesList m_recent_doc_files;
	bool m_record_output = false;
	int m_num_outchans = 2;
    double m_min_render_len = 5.0;
	void addParameterSliders();
	void showBubbleMessage(Component* toPointTo, String msg, int timeOut, bool isError);
	void updateParamLabelColors(int selmodule);
    std::vector<ValueTree> m_pars_snapshot;
	void storeSnapShot(int index);
	void recallSnapShot(int index);
	bool m_stoprequested = false;
    TooltipWindow m_tooltipwindow;
	std::vector<RemoteMapping> m_midimappings;
	int m_param_to_learn = -1;
	ValueTree saveMIDImappings();
	void restoreMIDImappings(ValueTree tree);
	bool m_harmonics_param_is_frequency = false;
	zoom_scrollbar m_waveformzs;
#ifdef USE_LUA_SCRIPTING
    sol::state m_lua_midi;
	sol::state m_lua_gui;
#endif
	ValueTree getStateAsTree();
	void restoreStateFromTree(ValueTree tree, bool exclude_importedfile);
	String saveStateAsFile(File f);
	
    OpenGLContext m_audvisogl;
	File m_cur_doc_file;
	File m_cur_lua_midi_file;
	bool m_lua_midi_enabled = false;
	File m_cur_lua_gui_file;
	std::mutex m_midi_mutex;
	double m_midi_tuning = 440.0;
	
	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(MainContentComponent)
};
