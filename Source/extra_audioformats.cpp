/*
 
 Copyright (C) 2017 Xenakios
 
 This program is free software; you can redistribute it and/or modify
 it under the terms of version 2 of the GNU General Public License
 as published by the Free Software Foundation.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License (version 2) for more details.
 
 You should have received a copy of the GNU General Public License (version 2)
 along with this program; if not, write to the Free Software Foundation,
 Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

#include "extra_audioformats.h"

extern std::unique_ptr<PropertiesFile> g_propsfile;
extern std::unique_ptr<AudioFormatManager> g_audioformatmanager;


FFMPEGAudioFileFormat::FFMPEGAudioFileFormat()
	: juce::AudioFormat(String("ffmpeg supported files"),
#if defined (JUCE_WINDOWS) || defined(JUCE_LINUX)
		StringArray
                        { ".wv",".ape",".mpc",".tak",".shn",".mp4", ".flv",".caf",".mkv",".avi",".ac3" })
#endif
#ifdef JUCE_MAC
	StringArray {
	".wv", ".ape", ".mpc", ".tak", ".shn", ".flv", ".mkv", ".avi", ".ac3"
})
#endif
{
}

String FFMPEGAudioFileFormat::getFFMPEGlocation()
{
#ifdef JUCE_WINDOWS
	String ffmpegloc = File::getSpecialLocation(File::currentExecutableFile).getParentDirectory().getFullPathName() + "/ffmpeg.exe";
#else
	String ffmpegloc = File::getSpecialLocation(File::currentExecutableFile).getParentDirectory().getFullPathName() + "/ffmpeg";
#endif
	File temploc(ffmpegloc);
	if (temploc.existsAsFile() == false)
		return String();
	return ffmpegloc;
}

Array<int> FFMPEGAudioFileFormat::getPossibleSampleRates()
{
	return Array<int>{22050, 24000, 44100, 48000, 88200, 96000, 176400, 192000};
}

Array<int> FFMPEGAudioFileFormat::getPossibleBitDepths()
{
	return Array<int>{8,16,24,32};
}

StringArray FFMPEGAudioFileFormat::getFileExtensions() const
{
	return AudioFormat::getFileExtensions();
}

bool FFMPEGAudioFileFormat::canDoStereo()
{
	return true;
}

bool FFMPEGAudioFileFormat::canDoMono()
{
	return true;
}

AudioFormatReader * FFMPEGAudioFileFormat::createReaderFor(InputStream * sourceStream, bool)
{
	std::unique_ptr<InputStream> holder(sourceStream);
    auto finstream = dynamic_cast<FileInputStream*>(sourceStream);
	if (finstream)
	{
		String filename = finstream->getFile().getFileName();
		auto modifdate = finstream->getFile().getLastModificationTime().toMilliseconds();
		auto filesize = finstream->getFile().getSize();
		MemoryBlock mb;
		mb.append((void*)filename.toRawUTF8(), filename.getNumBytesAsUTF8());
		mb.append((void*)&modifdate, sizeof(modifdate));
		mb.append((void*)&filesize, sizeof(filesize));
		SHA256 hash(mb);
		String outfilename = g_propsfile->getFile().getParentDirectory().getFullPathName() + "/converted_cache/" + hash.toHexString() + ".wav";
		File outfile(outfilename);
		if (outfile.existsAsFile())
		{
			return g_audioformatmanager->createReaderFor(outfile);
		}
		ChildProcess cp;
		StringArray args;
		args.add(getFFMPEGlocation());
		args.add("-i");
		args.add(finstream->getFile().getFullPathName());
		args.add(outfilename);
		if (cp.start(args) == false)
			Logger::writeToLog("could not start ffmpeg process");
		cp.waitForProcessToFinish(30000);
		if (cp.getExitCode() == 0)
		{
			return g_audioformatmanager->createReaderFor(outfile);
		}
		else
		{
			Logger::writeToLog(cp.readAllProcessOutput());
		}
	}
	return nullptr;
}

AudioFormatWriter * FFMPEGAudioFileFormat::createWriterFor(OutputStream * /*streamToWriteTo*/, 
	double /*sampleRateToUse*/, 
	unsigned int /*numberOfChannels*/, 
	int /*bitsPerSample*/, 
	const StringPairArray & /*metadataValues*/, 
	int /*qualityOptionIndex*/)
{
	return nullptr;
}
