/*
 
 Copyright (C) 2017 Xenakios
 
 This program is free software; you can redistribute it and/or modify
 it under the terms of version 2 of the GNU General Public License
 as published by the Free Software Foundation.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License (version 2) for more details.
 
 You should have received a copy of the GNU General Public License (version 2)
 along with this program; if not, write to the Free Software Foundation,
 Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

class FFMPEGAudioFileFormat : public AudioFormat
{
public:
	static String getFFMPEGlocation();
	FFMPEGAudioFileFormat();
	Array<int> getPossibleSampleRates() override;
	Array<int> getPossibleBitDepths() override;
	StringArray getFileExtensions() const override;
	bool canDoStereo() override;
	bool canDoMono() override;
	AudioFormatReader * createReaderFor(InputStream * sourceStream, bool deleteStreamIfOpeningFails) override;
	AudioFormatWriter * createWriterFor(OutputStream * streamToWriteTo, double sampleRateToUse, unsigned int numberOfChannels, int bitsPerSample, const StringPairArray & metadataValues, int qualityOptionIndex) override;
};

