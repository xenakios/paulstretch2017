#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "PS_Source/PaulStretchControl.h"

template<typename T>
inline bool is_in_range(T val, T start, T end)
{
	return val >= start && val <= end;
}

class zoom_scrollbar : public Component
{
public:
	enum hot_area
	{
		ha_none,
		ha_left_edge,
		ha_right_edge,
		ha_handle
	};
	void mouseDown(const MouseEvent& e) override;
	void mouseMove(const MouseEvent& e) override;
	void mouseDrag(const MouseEvent& e) override;
	void mouseEnter(const MouseEvent &event) override;
	void mouseExit(const MouseEvent &event) override;
	void paint(Graphics &g) override;
	std::function<void(Range<double>)> RangeChanged;
	Range<double> get_range() const { return m_therange; }
	void setRange(Range<double> rng, bool docallback);
private:
	Range<double> m_therange{ 0.0,1.0 };

	hot_area m_hot_area = ha_none;
	hot_area get_hot_area(int x, int y);
	int m_drag_start_x = 0;
};

template<typename CompType>
class ContextMenuComponent : public CompType
{
public:
	std::function<void(Component*)> PopMenuCallback;
	ContextMenuComponent() : CompType() { PopMenuCallback = [](Component*) {}; }
	void mouseDown(const MouseEvent& ev) override
	{
		if (ev.mods.isRightButtonDown() == true)
		{
			PopMenuCallback(this);
			return;
		}
		CompType::mouseDown(ev);
	}
};

class MyButton : public TextButton, public Button::Listener
{
public:
	using callback_t = std::function<void(void)>;
	MyButton(String txt, callback_t callback = []() {}) : TextButton()
	{
		setButtonText(txt);
		m_callback = callback;
		addListener(this);
	}
	void buttonClicked(Button*) override
	{
		if (m_callback)
			m_callback();
	}
	void setCallback(callback_t f)
	{
		m_callback = f;
	}
private:
	callback_t m_callback;
};

class WaveformComponent : public Component, public ChangeListener, public Timer
{
public:
	WaveformComponent();
	~WaveformComponent();
	void changeListenerCallback(ChangeBroadcaster* cb) override;
	void paint(Graphics& g) override;
	void setAudioFile(File f);
	void timerCallback() override;
	std::function<double()> CursorPosCallback;
	std::function<void(double)> SeekCallback;
	std::function<void(Range<double>, int)> TimeSelectionChangedCallback;
	void mouseDown(const MouseEvent& e) override;
	void mouseUp(const MouseEvent& e) override;
	void mouseDrag(const MouseEvent& e) override;
	void mouseMove(const MouseEvent& e) override;
	Range<double> getTimeSelection()
	{
		if (m_time_sel_start >= 0.0 && m_time_sel_end>m_time_sel_start + 0.001)
			return { m_time_sel_start, m_time_sel_end };
		return { 0.0, 1.0 };
	}
	void setTimeSelection(Range<double> rng)
	{
		if (rng.isEmpty())
			rng = { -1.0,1.0 };
		m_time_sel_start = rng.getStart();
		m_time_sel_end = rng.getEnd();
		repaint();
	}
	void setFileCachedRange(std::pair<Range<double>, Range<double>> rng);
	void setTimerEnabled(bool b);
	void setViewRange(Range<double> rng);
	Value ShowFileCacheRange;
private:
	AudioThumbnailCache m_thumbcache;

	std::unique_ptr<AudioThumbnail> m_thumb;
	Range<double> m_view_range{ 0.0,1.0 };
	int m_time_sel_drag_target = 0;
	double m_time_sel_start = -1.0;
	double m_time_sel_end = -1.0;
	double m_drag_time_start = 0.0;
	bool m_mousedown = false;
	bool m_didseek = false;
	bool m_didchangetimeselection = false;
	int m_topmargin = 20;
	int getTimeSelectionEdge(int x, int y);
	std::pair<Range<double>, Range<double>> m_file_cached;
	File m_curfile;
	Image m_waveimage;
    OpenGLContext m_ogl;
	bool m_use_opengl = false;
};

class PerfMeterComponent : public Component, public Timer, public SettableTooltipClient
{
public:
	PerfMeterComponent();
	void paint(Graphics& g) override;
	void setPSControl(Control* c);
	void timerCallback() override;
private:
	Control * m_control = nullptr;
	Font m_font;
};

class SpectralProcessesListBox : public ListBox,
	public ListBoxModel
{
public:
	SpectralProcessesListBox();
	void setPSControl(Control* c);
	void selectedRowsChanged(int lastRowSelected) override;
	int getNumRows() override;

	void paintListBoxItem(int rowNumber, Graphics &g, int width, int height, bool rowIsSelected) override;
	std::function<void(int)> RowClickedCallBack;
	void moveSelectedUpOrDown(bool up);
private:
	Control * m_PScontrol = nullptr;
	int m_drag_row = -1;
	std::vector<SpectrumProcess> m_spec_list;
	std::vector<int> m_spec_order;
};

class ParamComponent : public Component, public Slider::Listener, public Button::Listener, public ComboBox::Listener
{
public:
	ParamComponent(String id,String name, double minval, double maxval, double defval, double step = 0.0, bool notifyOnMouseUp = false);
	ParamComponent(String id,String name, Value& val, double minval, double maxval, double defval, double step = 0.0);
	ParamComponent(String id,String name, bool defval);
	ParamComponent(String id,String name, StringArray choices, int defaultchoice);
	void buttonClicked(Button* but) override;
	void comboBoxChanged(ComboBox* cb) override;
	void setHighlightColor(Colour c, Font f);
	void resized() override;
	void sliderValueChanged(Slider* slid) override;
	void sliderDragEnded(Slider* slid) override;
	std::function<void(double)> ValueChangedCallback;
	std::function<void(void)> AdditionalCallBack;
	int m_spec_module_index = -1;
	bool m_notif_on_up = false;
	Label m_label;
	void setValue(double x, bool isnormalized);
	double getValue() { return m_val; }
	double getValueFromNormalized(double x);
	void toggleBoolParameter()
	{
		if (m_togglebut != nullptr)
		{
			if (m_val < 0.5)
				setValue(1.0,false);
			else setValue(0.0,false);
		}
	}
	void setSkewFactor(double sf, bool b)
	{
		if (m_slider != nullptr)
			m_slider->setSkewFactor(sf, b);
	}
	void setSkewFactorFromMidPoint(double x)
	{
		if (m_slider != nullptr)
			m_slider->setSkewFactorFromMidPoint(x);
	}
	void setRange(double minval, double maxval, double step)
	{
		if (m_slider != nullptr)
			m_slider->setRange(minval, maxval, step);
	}
	void updateComponent();
	void setSliderAsMIDILearned(bool b);
	bool isSlider() { return m_slider != nullptr; }
	void setPopUpMenuCallback(std::function<void(Component*)> cb);
	std::unique_ptr<ContextMenuComponent<ToggleButton>> m_togglebut;
	std::unique_ptr<ContextMenuComponent<ComboBox>> m_combobox;
	String getID() { return m_id;  }
	bool m_learn_allowed = true;
private:
	std::unique_ptr<ContextMenuComponent<Slider>> m_slider;
	String m_id;
	Colour m_origslidercolor;
	double m_val = 0.0;
	
};
