/*
Copyright (C) 2006-2011 Nasca Octavian Paul
Author: Nasca Octavian Paul

Copyright (C) 2017 Xenakios

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License (version 2) for more details.

You should have received a copy of the GNU General Public License (version 2)
along with this program; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
*/

#include "../JuceLibraryCode/JuceHeader.h"
#include "MainComponent.h"
#ifdef WIN32
#include "windows.h"
//#define PS_WINDOWS_LIMIT_CORECOUNT;
#endif
#include "extra_audioformats.h"
std::unique_ptr<PropertiesFile> g_propsfile;
std::unique_ptr<AudioFormatManager> g_audioformatmanager;

String g_apptitle{ "PaulStretch3.0.0 (preview13)" };
String g_pf_ext{ ".paulstretch3" };

//==============================================================================
class PaulStretchJUCEApplication  : public JUCEApplication
{
public:
    //==============================================================================
    PaulStretchJUCEApplication() 
	{
	}

    const String getApplicationName() override       { return ProjectInfo::projectName; }
    const String getApplicationVersion() override    { return ProjectInfo::versionString; }
    bool moreThanOneInstanceAllowed() override       { return true; }

    void initialise (const String& /*commandLine*/) override
    {
#ifdef PS_WINDOWS_LIMIT_CORECOUNT
		HANDLE process = GetCurrentProcess();
		DWORD_PTR processAffinityMask = 0;
		processAffinityMask |= 1 << 0;
		//processAffinityMask |= 1 << 1;
		processAffinityMask |= 1 << 2;
		//processAffinityMask |= 1 << 3;
		BOOL success = SetProcessAffinityMask(process, processAffinityMask);
		if (success)
			Logger::writeToLog("Managed to change process CPU affinity");
#endif
		//Logger::writeToLog("FFTW init threads "+String(fftwf_init_threads()));
        g_audioformatmanager = std::make_unique<AudioFormatManager>();
        
		PropertiesFile::Options poptions;
		poptions.applicationName = "PaulStretch3";
		poptions.folderName = "PaulStretch3";
		poptions.commonToAllUsers = false;
		poptions.doNotSave = false;
		poptions.storageFormat = PropertiesFile::storeAsXML;
		poptions.millisecondsBeforeSaving = 1000;
		poptions.ignoreCaseOfKeyNames = false;
		poptions.processLock = nullptr;
		poptions.filenameSuffix = ".xml";
		poptions.osxLibrarySubFolder = "Application Support";

		g_propsfile = std::make_unique<PropertiesFile>(poptions);
		
		String cachefolder = g_propsfile->getFile().getParentDirectory().getFullPathName() + "/converted_cache";
        File tempdirfile(cachefolder);
        if (tempdirfile.exists() == false)
            tempdirfile.createDirectory();
        if (FFMPEGAudioFileFormat::getFFMPEGlocation().isNotEmpty())
			g_audioformatmanager->registerFormat(new FFMPEGAudioFileFormat, false);
		g_audioformatmanager->registerBasicFormats();
		auto cmdarray = getCommandLineParameterArray();
		/*
		if (cmdarray.size() > 1)
		{
			std::cout << "PaulStretch3 command line mode\n";
			setApplicationReturnValue(0);
			quit();
			PostQuitMessage(0);
			return;
		}
		*/
		mainWindow = std::make_unique<MainWindow>(getApplicationName());
		mainWindow->setResizable(true, false);
        mainWindow->getConstrainer()->setMinimumSize(1300, 780);
        int x = g_propsfile->getDoubleValue("window_x", mainWindow->getX());
		int y = g_propsfile->getDoubleValue("window_y", mainWindow->getY());
		int w = g_propsfile->getDoubleValue("window_w", mainWindow->getWidth());
		int h = g_propsfile->getDoubleValue("window_h", mainWindow->getHeight());
		mainWindow->setBounds(x, y, w, h);
        Timer::callAfterDelay(500, [this](){ mainWindow->m_content_comp->grabKeyboardFocus(); });
#ifndef DEBUG
        mainWindow->setName(g_apptitle);
#else
        mainWindow->setName(g_apptitle+" DEBUG");
#endif
		if (cmdarray.size() == 1)
		{
			File f(cmdarray[0]);
			if (f.existsAsFile())
			{
				if (f.getFileExtension() == g_pf_ext)
					mainWindow->m_content_comp->loadStateFromFile(f);
				else
					mainWindow->m_content_comp->setAudioFile(f);
			}
		}
		return;
		for (auto& e : cmdarray)
		{
			Logger::writeToLog(e);
			if (e.startsWith("-i"))
			{
				String temp = e.substring(2);
				File f(temp);
				if (f.existsAsFile())
					mainWindow->m_content_comp->setAudioFile(f);
			}
		}
		
	}

    void shutdown() override
    {
		if (mainWindow != nullptr)
		{
			g_propsfile->setValue("window_x", mainWindow->getX());
			g_propsfile->setValue("window_y", mainWindow->getY());
			g_propsfile->setValue("window_w", mainWindow->getWidth());
			g_propsfile->setValue("window_h", mainWindow->getHeight());
			mainWindow = nullptr;
		}
		g_propsfile = nullptr;
        g_audioformatmanager = nullptr;
		fftwf_cleanup();
	}

    //==============================================================================
    void systemRequestedQuit() override
    {
        quit();
    }

    void anotherInstanceStarted (const String& /*commandLine*/) override
    {
        
    }

    
    class MainWindow    : public DocumentWindow
    {
    public:
        MainWindow (String name)  : DocumentWindow (name,
                                                    Desktop::getInstance().getDefaultLookAndFeel()
                                                                          .findColour (ResizableWindow::backgroundColourId),
                                                    DocumentWindow::allButtons)
        {
            setUsingNativeTitleBar (true);
			m_content_comp = new MainContentComponent();
			setContentOwned (m_content_comp, true);

            centreWithSize (getWidth(), getHeight());
            setVisible (true);
        }
		MainContentComponent* m_content_comp = nullptr;
        void closeButtonPressed() override
        {
            JUCEApplication::getInstance()->systemRequestedQuit();
        }

    private:
        JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainWindow)
    };

private:
    std::unique_ptr<MainWindow> mainWindow;
};

START_JUCE_APPLICATION (PaulStretchJUCEApplication)
