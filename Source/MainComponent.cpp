/*
Copyright (C) 2006-2011 Nasca Octavian Paul
Author: Nasca Octavian Paul

Copyright (C) 2017 Xenakios

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License (version 2) for more details.

You should have received a copy of the GNU General Public License (version 2)
along with this program; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
*/

#include "MainComponent.h"
#include "RenderSettingsComponent.h"
#include "extra_audioformats.h"

extern std::unique_ptr<PropertiesFile> g_propsfile;
extern std::unique_ptr<AudioFormatManager> g_audioformatmanager;
extern String g_apptitle;
extern String g_pf_ext;

class LambdaPropertyButton : public ButtonPropertyComponent
{
public:
	LambdaPropertyButton(String propname, String buttxt, std::function<void(void)> cb)
		: ButtonPropertyComponent(propname,true), m_buttext(buttxt), m_cb(cb)
	{
	}
	
	void buttonClicked() override
	{
		m_cb();
	}

	String getButtonText() const override
	{
		return m_buttext;
	}
private:
	String m_buttext;
	std::function<void(void)> m_cb;
};

class PreferencesComponent : public Component, public Button::Listener
{
public:
    PreferencesComponent(AudioDeviceManager* adm)
    : m_audiodevicecomponent(*adm, 0, 0, 2, g_maxnumoutchans, true, false, true, false),
    m_tabcomp(TabbedButtonBar::TabsAtTop)
    {
        m_prebuf_amount = g_propsfile->getIntValue("prebufferamount",2);
        
        m_show_outputvisualizer = g_propsfile->getBoolValue("outputvis",true);
        m_prebufpriority = g_propsfile->getIntValue("prebufthreadpriority",5);
        
        m_fftw_allow_long_plan = g_propsfile->getBoolValue("fftw_allow_long_planning",false);
		m_showcachedinputfileranges = g_propsfile->getBoolValue("showfilecachedrange", false);
		
		m_recordliveoutput = false;
		addAndMakeVisible(&m_tabcomp);
        Array<PropertyComponent*> propcomps;
        
        propcomps.add(new ChoicePropertyComponent(m_prebuf_amount,"Prebuffering amount",
                                                  {"Very small","Small","Medium","Large","Very large","Huge"},{0,1,2,3,4,5}));
        propcomps.add(new ChoicePropertyComponent(m_prebufpriority,"Prebuffering thread priority",
                                                  {"Lower","Normal","Higher"},{4,5,6}));
        
        
		propcomps.add(new BooleanPropertyComponent(m_recordliveoutput, "Record live output", ""));
		propcomps.add(new LambdaPropertyButton("File for recording live output", "Choose output file...", [this]() 
		{ chooseLiveOutputFile(); }));
		propcomps.add(new BooleanPropertyComponent(m_show_outputvisualizer,"Show output visualizer",""));
        propcomps.add(new BooleanPropertyComponent(m_showcachedinputfileranges, "Show cached file ranges", ""));

		auto bcomp = new BooleanPropertyComponent(m_fftw_allow_long_plan, "Allow FFTW to\nplan (very) slowly", "");
		bcomp->setPreferredHeight(40);
		propcomps.add(bcomp);

		m_panel.addProperties(propcomps);
		
		String fftlib = fftwf_version;

		String juceversiontxt = String("JUCE ") + String(JUCE_MAJOR_VERSION) + "." + String(JUCE_MINOR_VERSION);
        String luaversiontxt = String(LUA_VERSION);
        String labeltxt =
			g_apptitle+"\n"
			"Application for extreme time stretching of sound files\nBuilt on " + String(__DATE__) + " " + String(__TIME__) + "\n"
			"Copyright (C) 2006-2011 Nasca Octavian Paul, Tg. Mures, Romania\n"
			"(C) 2017 Xenakios\n\n"+
			luaversiontxt+"\n\n"
            "Using " + fftlib + " for FFT\n\n"
			+ juceversiontxt + " (c) Roli. Used under the GPL license.\n\n"
			"GPL licensed source code for this application at : https://bitbucket.org/xenakios/paulstretch2017/src\n";

		m_aboutlabel.setJustificationType(Justification::topLeft);
		m_aboutlabel.setFont(20.0f);
		m_aboutlabel.setText(labeltxt, dontSendNotification);

        m_tabcomp.addTab("General", Colours::grey, &m_panel, false);
        m_tabcomp.addTab("Audio device", Colours::grey, &m_audiodevicecomponent, false);
		m_tabcomp.addTab("About", Colours::grey, &m_aboutlabel, false);
		addAndMakeVisible(&m_but_cancel);
		m_but_cancel.addListener(this);
		m_but_cancel.setButtonText("Cancel");
		addAndMakeVisible(&m_but_ok);
		m_but_ok.setButtonText("OK");
		m_but_ok.addListener(this);
    }
    ~PreferencesComponent()
    {
        
    }
	void chooseLiveOutputFile()
	{
		String lastexportfolder = g_propsfile->getValue("last_folder_export_file");
		FileChooser myChooser("Please select audio file to record into...",
			File(lastexportfolder),
			"*.wav");
		if (myChooser.browseForFileToSave(true))
		{
			m_liveoutfile = myChooser.getResult();
			g_propsfile->setValue("last_folder_export_file", m_liveoutfile.getParentDirectory().getFullPathName());
		}
	}
    
	void buttonClicked(Button* but) override
	{
		auto pardlg = dynamic_cast<DialogWindow*>(getParentComponent());
		if (pardlg != nullptr)
		{
			if (but == &m_but_cancel)
				pardlg->exitModalState(0);
			if (but == &m_but_ok)
				pardlg->exitModalState(1);
		}
	}
	void saveSettings()
	{
		g_propsfile->setValue("outputvis", (bool)m_show_outputvisualizer.getValue());
		
		g_propsfile->setValue("prebufferamount", (int)m_prebuf_amount.getValue());
		g_propsfile->setValue("prebufthreadpriority", (int)m_prebufpriority.getValue());
		g_propsfile->setValue("fftw_allow_long_planning", (bool)m_fftw_allow_long_plan.getValue());
		g_propsfile->setValue("showfilecachedrange", (bool)m_showcachedinputfileranges.getValue());
		
	}
    void resized() override
    {
        m_tabcomp.setBounds(1,1,getWidth()-1, getHeight()-26);
		m_but_cancel.changeWidthToFitText(24);
		m_but_ok.changeWidthToFitText(24);
		int butwidths = m_but_cancel.getWidth() + 1 + m_but_ok.getWidth() +1;
		m_but_cancel.setTopLeftPosition(getWidth() - butwidths, getHeight() - 25);
		m_but_ok.setTopLeftPosition(m_but_cancel.getRight() + 1, getHeight() - 25);
    }
    Value m_fftw_allow_long_plan;
    Value m_prebuf_amount;
    
    Value m_show_outputvisualizer;
    Value m_prebufpriority;
    
	Value m_showcachedinputfileranges;
	Value m_recordliveoutput;
	
	File m_liveoutfile;
	
private:
    TabbedComponent m_tabcomp;
    PropertyPanel m_panel;
    AudioDeviceSelectorComponent m_audiodevicecomponent;
	Label m_aboutlabel;
	TextButton m_but_ok;
	TextButton m_but_cancel;
};

MainContentComponent::MainContentComponent() :
	m_progressbar(m_render_progress),
	m_rewindbut("<<", [this]() { m_thecontrol->set_seek_pos(0.0); }),
	m_aviscomp(2),
	m_group_stretcher("group1", "Time stretch/Playback"),
	m_group_pitch("group2", "Pitch"),
	m_group_harmonics("group3", "Harmonics"),
	m_group_tonalvsnoise("group4", "Tonal vs Noise"),
	m_group_filter("group5", "Filter"),
	m_group_compressor("group6", "Compressor"),
	m_group_spread("group7", "Frequency spread"),
	m_group_freqshift("group8", "Frequency shift"),
	m_group_specorder("group9", "Spectral order")
	
{
	if (g_propsfile->getFile().existsAsFile())
    {
        File wisdomfile = g_propsfile->getFile().getSiblingFile("fftw_wisdom.txt");
        if (fftwf_import_wisdom_from_filename(wisdomfile.getFullPathName().toRawUTF8())==0)
            Logger::writeToLog("Could not load wisdom");
    }
	m_midi_tuning = g_propsfile->getDoubleValue("midinotetuning", 440.0);
#ifdef USE_LUA_SCRIPTING
	m_lua_midi.open_libraries();
    m_lua_midi["set_parameter"]=[this](int parindex, double value, bool normalized)
    {
        if (parindex>=0 && parindex<m_param_components.size())
        {
            m_param_components[parindex]->setValue(value,normalized);
        }
    };
	
	m_lua_midi_enabled = g_propsfile->getBoolValue("luamidienabled", false);
#endif
    addAndMakeVisible(&m_group_stretcher);
	addAndMakeVisible(&m_group_pitch);
	addAndMakeVisible(&m_group_harmonics);
	addAndMakeVisible(&m_group_tonalvsnoise);
	addAndMakeVisible(&m_group_filter);
	addAndMakeVisible(&m_group_compressor);
	addAndMakeVisible(&m_group_spread);
	addAndMakeVisible(&m_group_freqshift);
	addAndMakeVisible(&m_group_specorder);
	
	addAndMakeVisible(&m_waveformzs);
	
	

	m_recent_audio_files.setMaxNumberOfItems(50);
	m_recent_doc_files.setMaxNumberOfItems(20);

	m_thecontrol = std::make_unique<Control>(true);
    
	
	m_spec_listbox.RowClickedCallBack = [this](int index) { updateParamLabelColors(index); };
	m_spec_listbox.setPSControl(m_thecontrol.get());
	addAndMakeVisible(&m_spec_listbox);
	
	String recentfiles = g_propsfile->getValue("recent_files");
    m_recent_audio_files.restoreFromString(recentfiles);
	m_recent_audio_files.removeNonExistentFiles();

	recentfiles = g_propsfile->getValue("recent_doc_files");
	m_recent_doc_files.restoreFromString(recentfiles);
	m_recent_doc_files.removeNonExistentFiles();

	
	

	auto audiohwxl = unique_from_raw(g_propsfile->getXmlValue("audiohwsettings"));
	String audio_error;
	if (audiohwxl != nullptr)
	{
		audio_error = m_thecontrol->m_adm->initialise(0, 2, audiohwxl.get(),  true);
	}
	else
	{
		audio_error = m_thecontrol->m_adm->initialiseWithDefaultDevices(0, 2);
	}
	if (audio_error.isEmpty() == false)
	{
		AlertWindow::showMessageBox(AlertWindow::WarningIcon, "Error opening audio device", 
			audio_error);
	}
	else
	{
		if (m_thecontrol->m_adm->getCurrentAudioDevice())
			m_num_outchans = m_thecontrol->m_adm->getCurrentAudioDevice()->getActiveOutputChannels().countNumberOfSetBits();
		Logger::writeToLog("Num out channels from device " + String(m_num_outchans));
	}

	m_wavecomponent.ShowFileCacheRange = g_propsfile->getBoolValue("showfilecachedrange", false);
	m_waveformzs.RangeChanged = [this](Range<double> rng)
	{
		m_wavecomponent.setViewRange(rng);
	};

    
    
	int prebuffer = g_propsfile->getIntValue("prebufferamount", 2);
	m_thecontrol->setPreBufferAmount(prebuffer);

	m_progressbar.setColour(ProgressBar::backgroundColourId, Colours::black);
	m_progressbar.setColour(ProgressBar::foregroundColourId, Colours::cyan);
	addAndMakeVisible(&m_progressbar);
	m_progressbar.setVisible(false);

	m_aviscomp.setColours(Colours::black, Colours::darkgrey);
	addAndMakeVisible(&m_aviscomp);
    //m_audvisogl.attachTo(m_aviscomp);
    m_aviscomp.setRepaintRate(1);
	m_thecontrol->setAudioVisualizer(&m_aviscomp);
	m_aviscomp.setVisible(g_propsfile->getBoolValue("outputvis", true));

	m_wavecomponent.CursorPosCallback = [this]() 
	{
		m_wavecomponent.setFileCachedRange(m_thecontrol->getStretchAudioSource()->getFileCachedRangesNormalized());
		return m_thecontrol->getLivePlayPosition();
	};

	m_wavecomponent.SeekCallback = [this](double pos)
	{
		pos = jlimit(0.0, 1.0, pos);
		m_thecontrol->set_seek_pos((REALTYPE)pos);
	};

	m_wavecomponent.TimeSelectionChangedCallback = [this](Range<double> rng, int action)
	{
		//m_thecontrol->getStretchAudioSource()->setPlayRange(rng);
		updateInfoLabel();
		if (action == 1 && m_thecontrol->getStretchAudioSource()->isLoopingEnabled())
			m_thecontrol->getStretchAudioSource()->setPlayRange(rng, true);
	};

	addAndMakeVisible(&m_move_spec_down);
	m_move_spec_down.setButtonText("Down");
	m_move_spec_down.addListener(this);
	addAndMakeVisible(&m_move_spec_up);
	m_move_spec_up.setButtonText("Up");
	m_move_spec_up.addListener(this);

	addAndMakeVisible(&m_recentfilesbut);
	m_recentfilesbut.setButtonText("Recent audio files...");
	m_recentfilesbut.addListener(this);

	addAndMakeVisible(&m_wavecomponent);

    addAndMakeVisible(&m_importfilebut);
	m_importfilebut.setButtonText("File...");
	m_importfilebut.addListener(this);
	
	addAndMakeVisible(&m_renderbut);
	m_renderbut.addListener(this);
	m_renderbut.setButtonText("Render...");

    addAndMakeVisible(&m_menubut);
    m_menubut.addListener(this);
    m_menubut.setButtonText("Settings...");
    
	addParameterSliders();
	m_spec_listbox.selectRow(0);
	m_thecontrol->ppar.spread.enabled = false;

	m_thecontrol->ppar.pitch_shift.enabled = true;
	m_thecontrol->ppar.octave.enabled = true;
    m_thecontrol->ppar.compressor.enabled = true;
    m_thecontrol->ppar.tonal_vs_noise.enabled = false;
    m_thecontrol->ppar.freq_shift.enabled = true;
    m_thecontrol->ppar.filter.enabled = true;
    m_thecontrol->ppar.harmonics.enabled = false;
    
	
	
	addAndMakeVisible(&m_stretchinfolabel);
	
	addAndMakeVisible(&m_playbut);
	m_playbut.setButtonText("Play");
	m_playbut.setEnabled(false);

	addAndMakeVisible(&m_rewindbut);
	//m_rewindbut.addListener(this);
	//m_rewindbut.setButtonText("<<");

	m_playbut.addListener(this);

	//addAndMakeVisible(&m_cpulabel);
    m_perfcomponent.setPSControl(m_thecontrol.get());
    addAndMakeVisible(&m_perfcomponent);
    
	setSize (1000, 600);
	startTimer(1, 200);
	startTimer(2, 50);
	auto midiccxml = unique_from_raw(g_propsfile->getXmlValue("midimappings"));
	if (midiccxml != nullptr)
	{
		ValueTree vt = ValueTree::fromXml(*midiccxml);
		if (vt.isValid())
			restoreMIDImappings(vt);
	}
	if (g_propsfile->containsKey("luamidiscriptfile"))
	{
		m_cur_lua_midi_file = File(g_propsfile->getValue("luamidiscriptfile"));
		loadLuaMIDIScript(m_cur_lua_midi_file);
	}
	m_thecontrol->m_adm->addMidiInputCallback(String(), this);
}

MainContentComponent::~MainContentComponent()
{
	m_thecontrol->stopplay();
    //m_audvisogl.detach();
    auto audiohwxml = unique_from_raw(m_thecontrol->m_adm->createStateXml());
	if (audiohwxml != nullptr)
	{
		g_propsfile->setValue("audiohwsettings", audiohwxml.get());
	}
	for (auto& e : m_param_components)
	{
		
        double val = e->getValue();
        g_propsfile->setValue(e->getID(), val);
    }
	g_propsfile->setValue("prebufferamount", m_thecontrol->getPreBufferAmount());
    g_propsfile->setValue("recent_files", m_recent_audio_files.toString());
	g_propsfile->setValue("recent_doc_files", m_recent_doc_files.toString());
	g_propsfile->setValue("outputvis", m_aviscomp.isVisible());
	g_propsfile->setValue("luamidiscriptfile", m_cur_lua_midi_file.getFullPathName());
	g_propsfile->setValue("luamidienabled", m_lua_midi_enabled);
	g_propsfile->setValue("showfilecachedrange", m_wavecomponent.ShowFileCacheRange);

	if (g_propsfile->getFile().existsAsFile())
    {
        File wisdomfile = g_propsfile->getFile().getSiblingFile("fftw_wisdom.txt");
        //Logger::writeToLog(wisdomfile.getFullPathName());
        fftwf_export_wisdom_to_filename(wisdomfile.getFullPathName().toRawUTF8());
    }

	ValueTree midiccstate = saveMIDImappings();
	auto xml = unique_from_raw(midiccstate.createXml());
	if (xml != nullptr)
	{
		g_propsfile->setValue("midimappings", xml.get());
	}
}

bool MainContentComponent::keyPressed(const juce::KeyPress &ev)
{
	/*
	if (ev.getModifiers().isCommandDown() && ev.getKeyCode() >= '1' && ev.getKeyCode() <='9')
	{
		Logger::writeToLog("Storing pars snapshot "+String(ev.getKeyCode()-48));
		storeSnapShot(ev.getKeyCode()-48);
		return true;
	}
	*/
	if (ev == KeyPress('0', ModifierKeys::noModifiers,0 )) 
	{
		//Logger::writeToLog("Recalling pars snapshot "+String(ev.getKeyCode()-48));
		recallSnapShot(ev.getKeyCode()-48);
		return true;
	}
	if (ev == KeyPress('+', ModifierKeys::commandModifier, 0))
	{
		m_waveformzs.setRange(m_wavecomponent.getTimeSelection(), true);
		return true;
	}
	if (ev == KeyPress('-', ModifierKeys::commandModifier, 0))
	{
		m_waveformzs.setRange({ 0.0,1.0 }, true);
		return true;
	}
	if (ev == KeyPress('s', ModifierKeys::commandModifier, 0))
	{
		saveDocumentAs();
        return true;
	}
	if (ev == KeyPress('o', ModifierKeys::commandModifier, 0))
	{
		openDocument();
        return true;
	}
#ifdef USE_LUA_SCRIPTING
	if (ev == KeyPress(KeyPress::F8Key, ModifierKeys::commandModifier, 0))
	{
		String lastexportfolder = g_propsfile->getValue("last_folder_midi_script_file");
		FileChooser myChooser("Please select Lua script...",
			File(lastexportfolder),
			"*.lua");
		if (myChooser.browseForFileToOpen())
		{
			m_cur_lua_midi_file = myChooser.getResult();
			g_propsfile->setValue("last_folder_midi_script_file", m_cur_lua_midi_file.getParentDirectory().getFullPathName());
			loadLuaMIDIScript(m_cur_lua_midi_file);
		}
		return true;
	}
	if (ev == KeyPress(KeyPress::F8Key,ModifierKeys::shiftModifier,0))
	{
		if (m_cur_lua_midi_file != File())
		{
			loadLuaMIDIScript(m_cur_lua_midi_file);
		}
		return true;
	}
	if (ev == KeyPress(KeyPress::F8Key, ModifierKeys::noModifiers, 0))
	{
		m_lua_midi_enabled = !m_lua_midi_enabled;
		m_stretchinfolabel.setText("Lua MIDI enabled " + String((int)m_lua_midi_enabled),dontSendNotification);
		return true;
	}
	if (ev == KeyPress(KeyPress::F9Key, ModifierKeys::commandModifier, 0))
	{
		String lastexportfolder = g_propsfile->getValue("last_folder_gui_script_file");
		FileChooser myChooser("Please select Lua script...",
			File(lastexportfolder),
			"*.lua");
		if (myChooser.browseForFileToOpen())
		{
			m_cur_lua_gui_file = myChooser.getResult();
			g_propsfile->setValue("last_folder_gui_script_file", m_cur_lua_gui_file.getParentDirectory().getFullPathName());
			loadLuaGUIScript(m_cur_lua_gui_file);
		}
		return true;
	}
	if (ev == KeyPress(KeyPress::F9Key, ModifierKeys::shiftModifier, 0))
	{
		if (m_cur_lua_gui_file != File())
		{
			loadLuaGUIScript(m_cur_lua_gui_file);
		}
		return true;
	}
#endif
	if (ev == KeyPress::spaceKey)
    {
        buttonClicked(&m_playbut);
        return true;
    }
    if (ev == KeyPress('f',ModifierKeys::noModifiers,0))
    {
		m_param_components[24]->toggleBoolParameter();
		return true;
    }
	if (ev == KeyPress::homeKey)
	{
		buttonClicked(&m_rewindbut);
		return true;
	}
    return false;
}

bool MainContentComponent::isInterestedInFileDrag (const StringArray &files)
{
	if (m_importfilebut.isEnabled() == false)
		return false;
	if (files.size()==0)
        return false;
    File f(files[0]);
    String extension = f.getFileExtension().toLowerCase();
	if (extension == g_pf_ext)
	{
		return true;
	}
	for (auto& e : *g_audioformatmanager)
    {
        if (e->getFileExtensions().contains(extension))
            return true;
    }
    return false;
}

void MainContentComponent::filesDropped (const StringArray &files, int, int)
{
	if (files.size() > 0)
	{
		File f(files[0]);
		if (f.getFileExtension() == g_pf_ext)
		{
			loadStateFromFile(f);
			toFront(true);
			return;
		}
		setAudioFile(f);
		toFront(true);
	}
}

void MainContentComponent::paint (Graphics& g)
{
    g.fillAll (getLookAndFeel().findColour (ResizableWindow::backgroundColourId));
}

void MainContentComponent::buttonClicked(Button* but)
{
	if (but == &m_recentfilesbut)
	{
		PopupMenu recentfilesmenu;
		m_recent_audio_files.createPopupMenuItems(recentfilesmenu, 10000, false, true);
		int r = recentfilesmenu.show();
		if (r >= 10000 && r<20000)
		{
			setAudioFile(m_recent_audio_files.getFile(r - 10000));
		}
	}
	if (but == &m_move_spec_down)
		m_spec_listbox.moveSelectedUpOrDown(false);
	if (but == &m_move_spec_up)
		m_spec_listbox.moveSelectedUpOrDown(true);
	if (but == &m_importfilebut)
	{
		PopupMenu filemenu;
		filemenu.addItem(1,"Import audio file...", true, false);
		filemenu.addSeparator();
		filemenu.addItem(2, "Open project...", true, false);
		filemenu.addItem(3, "Save project as...", true, false);
		filemenu.addItem(4, "Save project", m_cur_doc_file != File(), false);
		PopupMenu recentdocfilesmenu;
		m_recent_doc_files.createPopupMenuItems(recentdocfilesmenu, 10000, true, true);
		filemenu.addSubMenu("Recent projects", recentdocfilesmenu, true);
		int r = filemenu.show();
		if (r == 1)
		{
			String importfilelastfolder = g_propsfile->getValue("last_folder_import_file");
			File startlocation(importfilelastfolder);
			String filterstring = g_audioformatmanager->getWildcardForAllFormats();
			//Logger::writeToLog(filterstring);
			FileChooser myChooser("Please select audio file...",
				startlocation,
				filterstring);

			if (myChooser.browseForFileToOpen())
			{
				setAudioFile(myChooser.getResult());
				File folder = myChooser.getResult().getParentDirectory();
				g_propsfile->setValue("last_folder_import_file", folder.getFullPathName());

				getParentComponent()->toFront(true);
			}
		}
		if (r >= 10000 && r < 20000)
		{
			File filetoload = m_recent_doc_files.getFile(r - 10000);
			if (loadStateFromFile(filetoload).isEmpty())
			{
				m_recent_doc_files.addFile(filetoload);
				m_cur_doc_file = filetoload;
			}
		}
		if (r == 2)
		{
			openDocument();
		}
		if (r == 3)
		{
			saveDocumentAs();
		}
		if (r == 4)
		{
			saveDocument();
		}
	}
	if (but == &m_renderbut)
	{
		showRenderWindow();
	}
	
	if (but == &m_playbut)
	{
		if (m_thecontrol->get_input_filename().isEmpty())
			return;
		if (m_thecontrol->playing() == false)
		{
            startPlay();
        }
		else
		{
            stopPlay();
        }
		
	}
	if (but == &m_rewindbut)
	{
		m_thecontrol->set_seek_pos(0.0);
	}
    if (but == &m_menubut)
    {
		showMiscMenu();
	}
}

void MainContentComponent::showAboutScreen()
{

    String fftlib = fftwf_version;

    String juceversiontxt = String("JUCE ") + String(JUCE_MAJOR_VERSION) + "." + String(JUCE_MINOR_VERSION);
	AlertWindow::showMessageBoxAsync(AlertWindow::InfoIcon,
		g_apptitle,
		"Application for extreme time stretching of sound files\nBuilt on " + String(__DATE__) + " "+String(__TIME__)+"\n"
		"Copyright (C) 2006-2011 Nasca Octavian Paul, Tg. Mures, Romania\n"
		"(C) 2017 Xenakios\n\n"
        "Using "+fftlib+" for FFT\n\n"
		+ juceversiontxt + " (c) Roli. Used under the GPL license.\n\n"
		"GPL licensed source code for this application at : https://bitbucket.org/xenakios/paulstretch2017/src\n"
		, "OK",
		this);
}

void MainContentComponent::showMiscMenu()
{
    stopPlay();
    PreferencesComponent rcomp(m_thecontrol->m_adm.get());
	rcomp.m_recordliveoutput.setValue(m_record_output);
    rcomp.setSize(580, 500);
    int dlgresult = DialogWindow::showModalDialog("Settings", &rcomp, this, Colours::darkgrey, true);
	if (dlgresult == 1)
	{
		rcomp.saveSettings();
		double curfftsize = m_param_components[2]->getValue();
		m_thecontrol->setPreBufferAmount(rcomp.m_prebuf_amount.getValue());
		m_thecontrol->setPrebufferThreadPriority(rcomp.m_prebufpriority.getValue());
		m_thecontrol->setFFTSize(curfftsize);
		updateInfoLabel();
		
		m_aviscomp.setVisible(rcomp.m_show_outputvisualizer.getValue());
		m_wavecomponent.ShowFileCacheRange = rcomp.m_showcachedinputfileranges.getValue();
		resized();
		if (rcomp.m_liveoutfile != File())
		{
			m_liverecordfile = rcomp.m_liveoutfile;
		}
		m_record_output = (bool)rcomp.m_recordliveoutput.getValue();
		if (m_record_output == false)
			m_thecontrol->setOutputAudioFileToRecord(File());
		if (m_thecontrol->m_adm->getCurrentAudioDevice())
			m_num_outchans = m_thecontrol->m_adm->getCurrentAudioDevice()->getActiveOutputChannels().countNumberOfSetBits();
		return;
	}
	if (dlgresult == 0)
	{
		return;
	}
}

void MainContentComponent::updateStretchParamsFromSliders()
{
	m_thecontrol->set_stretch_controls(m_param_components[1]->getValue(),
		0,
		m_param_components[2]->getValue(),
		m_param_components[3]->getValue());
	m_thecontrol->update_player_stretch();
	updateInfoLabel();
}

void MainContentComponent::addParameterSliders()
{
	m_param_components.push_back(std::make_shared<ParamComponent>("mainvol0","Main volume",
		m_thecontrol->getStretchAudioSource()->val_MainVolume, -24.0, 12.0, -9.0, 0.5)); // 0
	
	
	m_param_components.push_back(std::make_shared<ParamComponent>("stretch0", "Stretch amount", 0.1, 1024.0, 4.0, 0.01)); // 1
	m_param_components.back()->setSkewFactor(0.5, false);
	m_param_components.back()->ValueChangedCallback = [this](double x)
	{
		m_thecontrol->setStretchAmount(x);
		updateInfoLabel();
		//updateStretchParamsFromSliders();
	};
	m_param_components.push_back(std::make_shared<ParamComponent>("fftsize0", "FFT size", 0.0, 1.0, 0.6, 0.01, true)); // 2
	m_param_components.back()->m_learn_allowed = false;
	m_param_components.back()->m_label.setTooltip("Low values keep more of the input sound's timing but result in rougher frequency content.\nHigh values keep more of the input sound's frequency content but lose timing accuracy.");
    m_param_components.back()->ValueChangedCallback = [this](double x)
	{
		m_thecontrol->setFFTSize(x);
		updateInfoLabel();
		//updateStretchParamsFromSliders();
	};
    
    m_param_components.push_back(std::make_shared<ParamComponent>("onsetdet0", "Onset detection", 0, 1.0, 0.0, 0.01)); // 3
	m_param_components.back()->ValueChangedCallback = [this](double x)
	{
		m_thecontrol->setOnsetDetection(x);
	};
	m_param_components.push_back(std::make_shared<ParamComponent>("pitchshift0", "Pitch shift", -24.0, 24.0, 0.0, 0.1));
	m_param_components.back()->m_label.setTooltip("Transposes the pitch of the sound, measured in semitones.\nAttempts to keep the harmonic frequency relations in the spectrum.");
    m_param_components.back()->m_spec_module_index = 3;
	m_param_components.back()->ValueChangedCallback = [this](double x)
	{
		m_thecontrol->ppar.pitch_shift.cents = (int)(x * 100);
	};
	m_param_components.push_back(std::make_shared<ParamComponent>("freqshift0", "Frequency shift", -1000.0, 1000.0, 0.0, 1.0));
	m_param_components.back()->m_label.setTooltip("Shifts the frequencies of the sound.\nNon zero values produce an inharmonic result, different compared to pitch shift.");
    m_param_components.back()->m_spec_module_index = 2;
	m_param_components.back()->ValueChangedCallback = [this](double x)
	{
		m_thecontrol->ppar.freq_shift.Hz = (int)x;
	};
	m_param_components.push_back(std::make_shared<ParamComponent>("octavem2level0", "Octave -2 level", 0.0, 1.0, 0.0, 0.01));
    m_param_components.back()->m_label.setTooltip("Mixes in the sound transposed down 2 octaves");
    m_param_components.back()->m_spec_module_index = 4;
	m_param_components.back()->ValueChangedCallback = [this](double x)
	{
		m_thecontrol->ppar.octave.om2 = (REALTYPE)x;
	};
	m_param_components.push_back(std::make_shared<ParamComponent>("octavem1level0", "Octave -1 level", 0.0, 1.0, 0.0, 0.01));
	m_param_components.back()->m_label.setTooltip("Mixes in the sound transposed down 1 octave");
    m_param_components.back()->m_spec_module_index = 4;
	m_param_components.back()->ValueChangedCallback = [this](double x)
	{
		m_thecontrol->ppar.octave.om1 = (REALTYPE)x;
	};
	m_param_components.push_back(std::make_shared<ParamComponent>("octave0level0", "Octave 0 level", 0.0, 1.0, 1.0, 0.01));
	m_param_components.back()->m_label.setTooltip("Mixes in the sound");
    m_param_components.back()->m_spec_module_index = 4;
	m_param_components.back()->ValueChangedCallback = [this](double x)
	{
		m_thecontrol->ppar.octave.o0 = (REALTYPE)x;
	};
	m_param_components.push_back(std::make_shared<ParamComponent>("octave1level0", "Octave +1 level", 0.0, 1.0, 0.0, 0.01));
	m_param_components.back()->m_label.setTooltip("Mixes in the sound transposed up 1 octave");
    m_param_components.back()->m_spec_module_index = 4;
	m_param_components.back()->ValueChangedCallback = [this](double x)
	{
		m_thecontrol->ppar.octave.o1 = (REALTYPE)x;
	};
	m_param_components.push_back(std::make_shared<ParamComponent>("octave15level0", "Octave +1.5 level", 0.0, 1.0, 0.0, 0.01));
	m_param_components.back()->m_label.setTooltip("Mixes in the sound transposed up 1 octave and a 5th");
    m_param_components.back()->m_spec_module_index = 4;
	m_param_components.back()->ValueChangedCallback = [this](double x)
	{
		m_thecontrol->ppar.octave.o15 = (REALTYPE)x;
	};
	m_param_components.push_back(std::make_shared<ParamComponent>("octave2level0", "Octave +2 level", 0.0, 1.0, 0.0, 0.01));
	m_param_components.back()->m_label.setTooltip("Mixes in the sound transposed up 2 octaves");
    m_param_components.back()->m_spec_module_index = 4;
	m_param_components.back()->ValueChangedCallback = [this](double x)
	{
		m_thecontrol->ppar.octave.o2 = (REALTYPE)x;
	};


	m_param_components.push_back(std::make_shared<ParamComponent>("compress0", "Compress", 0.0, 1.0, 0.0, 0.01));
	m_param_components.back()->m_spec_module_index = 7;
    m_param_components.back()->m_label.setTooltip("Compresses the spectrum volumes.\nNon zero values typically add noise and loudness to the sound.");
    m_param_components.back()->ValueChangedCallback = [this](double x)
	{
		m_thecontrol->ppar.compressor.power = (REALTYPE)x;
	};
	m_param_components.push_back(std::make_shared<ParamComponent>("spread0", "Frequency spread", 0.0, 1.0, 0.0, 0.01));
	m_param_components.back()->m_spec_module_index = 5;
	m_param_components.back()->ValueChangedCallback = [this](double x)
	{
		if (x < 0.05)
		{
			m_thecontrol->ppar.spread.enabled = false;
		}
		else
		{
			m_thecontrol->ppar.spread.enabled = true;
			m_thecontrol->ppar.spread.bandwidth = (REALTYPE)jmap(x, 0.05, 1.0, 0.0, 1.0);
		}
	};

	m_param_components.push_back(std::make_shared<ParamComponent>("tonnoibw0", "Tonal/noise bandwidth", 0.74, 1.0, 0.74, 0.01)); // 14
	m_param_components.back()->m_spec_module_index = 1;
	m_param_components.back()->ValueChangedCallback = [this](double x)
	{
		if (x >= 0.75)
		{
			m_thecontrol->ppar.tonal_vs_noise.enabled = true;
			m_thecontrol->ppar.tonal_vs_noise.bandwidth = (REALTYPE)x;
			m_param_components[15]->setEnabled(true);
		}
		else
		{
			m_thecontrol->ppar.tonal_vs_noise.enabled = false;
			m_param_components[15]->setEnabled(false);
		}
	};
	m_param_components.push_back(std::make_shared<ParamComponent>("tonnoipreser0", "Tonal/noise preserve", -1.0, 1.0, 0.5,0.01)); // 15
	m_param_components.back()->m_spec_module_index = 1;
	m_param_components.back()->ValueChangedCallback = [this](double x)
	{
		m_thecontrol->ppar.tonal_vs_noise.preserve = (REALTYPE)x;
	};

	m_param_components.push_back(std::make_shared<ParamComponent>("filterlow0", "Filter low", 20.0, 10000.0, 20.0, 1.0)); // 16
	m_param_components.back()->m_spec_module_index = 6;
	m_param_components.back()->setSkewFactorFromMidPoint(1000.0);
	m_param_components.back()->ValueChangedCallback = [this](double x)
	{
		m_thecontrol->ppar.filter.low = (REALTYPE)x;
	};

	m_param_components.push_back(std::make_shared<ParamComponent>("filterhigh0", "Filter high", 100.0, 25000, 22000.0, 1.0)); // 17
	m_param_components.back()->m_spec_module_index = 6;
	m_param_components.back()->setSkewFactorFromMidPoint(1000.0);
	m_param_components.back()->ValueChangedCallback = [this](double x)
	{
		m_thecontrol->ppar.filter.high = (REALTYPE)x;
	};
    
    m_param_components.push_back(std::make_shared<ParamComponent>("numharmonics0", "Num harmonics", 0.0, 100.0, 0.0, 1.0)); // 18
	m_param_components.back()->m_spec_module_index = 0;
	m_param_components.back()->ValueChangedCallback = [this](double x)
    {
        if (x>=1.0)
        {
            m_thecontrol->ppar.harmonics.nharmonics = (int)x;
            m_thecontrol->ppar.harmonics.enabled = true;
            m_param_components[19]->setEnabled(true);
            m_param_components[20]->setEnabled(true);
            m_param_components[21]->setEnabled(true);
        } else
        {
            m_thecontrol->ppar.harmonics.enabled = false;
            m_param_components[19]->setEnabled(false);
            m_param_components[20]->setEnabled(false);
            m_param_components[21]->setEnabled(false);
        }
    };
	if (m_harmonics_param_is_frequency==false)
		m_param_components.push_back(std::make_shared<ParamComponent>("harmfreq0", "Harm freq (MIDI note)", 0.0, 127.0, 32.0, 1.0)); // 19
	else
	{
		m_param_components.push_back(std::make_shared<ParamComponent>("harmfreq0", "Harm freq (Hz)", 8.0, 5000.0, 64.0, 1.0)); // 19
		m_param_components.back()->setSkewFactorFromMidPoint(1000.0);
	}
	m_param_components.back()->m_spec_module_index = 0;
	
    m_param_components.back()->ValueChangedCallback = [this](double x)
    {
		if (m_harmonics_param_is_frequency == false)
			m_thecontrol->ppar.harmonics.freq = (REALTYPE)(m_midi_tuning * pow(1.05946309436,x-69));
		else m_thecontrol->ppar.harmonics.freq = (REALTYPE)x;
        //m_thecontrol->ppar.harmonics.enabled = m_param_components[18]->getValue()>1.0;
    };
    
    m_param_components.push_back(std::make_shared<ParamComponent>("harmbw0", "Harmonics bandwidth", 0.1, 200.0, 25.0, 1.0)); // 20
	m_param_components.back()->m_spec_module_index = 0;
	m_param_components.back()->ValueChangedCallback = [this](double x)
    {
        m_thecontrol->ppar.harmonics.bandwidth=(REALTYPE)x;
        //m_thecontrol->ppar.harmonics.enabled = m_param_components[18]->getValue()>1.0;
    };
    
    m_param_components.push_back(std::make_shared<ParamComponent>("harmsmooth0", "Harmonics smooth", false)); // 21
	m_param_components.back()->m_spec_module_index = 0;
	m_param_components.back()->ValueChangedCallback = [this](double x)
    {
        m_thecontrol->ppar.harmonics.gauss = x>0.5;
        //m_thecontrol->ppar.harmonics.enabled = m_param_components[18]->getValue()>1.0;
    };
    
    m_param_components.push_back(std::make_shared<ParamComponent>("loopxfade0", "Loop XFade seconds",
                                                               m_thecontrol->getStretchAudioSource()->val_XFadeLen, 0.0, 1.0, 0.0, 0.01)); // 22
   
    m_param_components.push_back(std::make_shared<ParamComponent>("fftwindowtype0", "FFT windowing type",
                                                               StringArray{"Rectangular","Hamming","Hann","Blackman","Blackman-Harris"},1)); // 23
    m_param_components.back()->m_label.setTooltip("Apart from the Rectangular mode, this has a pretty subtle effect on the sound.\nHamming is recommended for most use.");
    m_param_components.back()->ValueChangedCallback = [this](double x)
    {
        m_thecontrol->getStretchAudioSource()->setFFTWindowingType((int)x);
    };
    
	m_param_components.push_back(std::make_shared<ParamComponent>("freezetoggle0", "Freeze", false)); // 24
	m_param_components.back()->ValueChangedCallback = [this](double x)
	{
		m_thecontrol->getStretchAudioSource()->setFreezing(x > 0.5);
	};

	m_param_components.push_back(std::make_shared<ParamComponent>("looptoggle0", "Loop", false)); // 25
	m_param_components.back()->m_learn_allowed = false;
	m_param_components.back()->ValueChangedCallback = [this](double x)
	{
		m_thecontrol->getStretchAudioSource()->setLoopingEnabled(x > 0.5);
	};

    storeSnapShot(0); // store reset snapshot
	int i = 0;
	for (auto& e : m_param_components)
	{
		addAndMakeVisible(e.get());
		
			ParamComponent* pcomp = e.get();
			e->setPopUpMenuCallback([i,this, pcomp](Component* /*sl*/)
			{
				PopupMenu menu;
				int found = -1;
				int j = 0;
				for (auto& ccmap : m_midimappings)
				{
					if (ccmap.m_dest == i)
					{
						found = ccmap.m_midicc;
						break;
					}
					++j;
				}
				if (found == -1 && m_param_to_learn == -1)
				{
					if (pcomp->m_learn_allowed)
						menu.addItem(1, "MIDI CC learn", true, false);
				}
				Component* contcomp = nullptr;
				EnvelopeComponent* envcomp = nullptr;
				if (found >= 0 && pcomp->m_learn_allowed)
				{
					menu.addItem(2, "Unlearn MIDI CC", true, false);
					contcomp = new Component;
					envcomp = new EnvelopeComponent;
					envcomp->ValueFromNormalized = [pcomp](double xnorm) 
					{
						return pcomp->getValueFromNormalized(xnorm);
					};
					contcomp->addAndMakeVisible(envcomp);
					int evcompw = 400;
					int evcomph = 400;
					int margin = 30;
					envcomp->setBounds(margin, margin, evcompw-margin*2, evcomph-margin*2);
					envcomp->set_envelope(m_midimappings[j].m_map_env,
						"Mapping curve for MIDI CC "+String(m_midimappings[j].m_midicc)+" on chan "
						+String(m_midimappings[j].m_midichan));
					menu.addCustomItem(10000, contcomp, evcompw, evcomph, false);
				}
				if (m_param_to_learn >= 0 && pcomp->m_learn_allowed)
					menu.addItem(3, "Cancel MIDI learn", true, false);
				if (i == 19)
				{
					menu.addItem(10, "Use Hz for frequency", true, m_harmonics_param_is_frequency);
					menu.addItem(11, "Set tuning...", true, false);
				}
				int r = menu.show();
				delete envcomp;
				delete contcomp;
				if (r == 10)
				{
					m_harmonics_param_is_frequency = !m_harmonics_param_is_frequency;
					if (m_harmonics_param_is_frequency == false)
					{
						pcomp->m_label.setText("Harm freq (MIDI note)", dontSendNotification);
						pcomp->setRange(0.0, 127.0, 1.0);
						pcomp->setSkewFactorFromMidPoint(64.0);
					}
					else
					{
						pcomp->m_label.setText("Harm freq (Hz)", dontSendNotification);
						pcomp->setRange(8.0, 5000.0, 1.0);
						pcomp->setSkewFactorFromMidPoint(1000.0);
					}
				}
				if (r == 11)
				{
					AlertWindow aw("PaulStretch3", "Set note A tuning", AlertWindow::QuestionIcon, this);
					aw.addTextEditor("hzvalue", String(m_midi_tuning), "Frequency");
					aw.addButton("OK", 1);
					aw.runModalLoop();
					m_midi_tuning = jlimit(300.0, 600.0, aw.getTextEditorContents("hzvalue").getDoubleValue());
					g_propsfile->setValue("midinotetuning", m_midi_tuning);
					pcomp->setValue(pcomp->getValue(), false);
				}
				if (r == 1)
				{
					m_param_to_learn = i;
					m_stretchinfolabel.setText("Learning "+pcomp->m_label.getText(),dontSendNotification);
				}
				if (r == 2)
				{
					pcomp->setSliderAsMIDILearned(false);
					std::lock_guard<std::mutex> locker(m_midi_mutex);
					m_midimappings.erase(m_midimappings.begin() + j);
				}
				if (r == 3)
				{
					m_param_to_learn = -1;
					updateInfoLabel();
				}
			});
		
		double val = e->getValue();
		auto parid = e->getID();
		double storedval = g_propsfile->getDoubleValue(parid, val);
		e->setValue(storedval, false);
		if (e->ValueChangedCallback)
			e->ValueChangedCallback(storedval);
		e->AdditionalCallBack = [this]() {m_thecontrol->update_process_parameters(); };
		++i;
	}
	MessageManager::callAsync([this]() 
	{
		m_thecontrol->ppar.harmonics.enabled = m_param_components[18]->getValue() > 1.0;
		m_thecontrol->update_process_parameters();
	});
}

void MainContentComponent::showBubbleMessage(Component * toPointTo, String msg, int timeOut, bool isError)
{
	BubbleMessageComponent* bub = new BubbleMessageComponent;
	if (msg.length() > 2048)
    {
		if (isError)
			msg = "Error message too long to show";
		else
			msg = "Message too long to show";
    }
	AttributedString attstring(msg);
	if (isError == true)
	{
		attstring.setFont(18.0f);
		attstring.setColour(Colours::white);
		bub->setColour(BubbleComponent::backgroundColourId, Colours::crimson);
	}
	bub->addToDesktop(0);
	bub->showAt(toPointTo, attstring, timeOut, true, true);
}

void MainContentComponent::updateParamLabelColors(int selmodule)
{
	Colour defcolor = m_stretchinfolabel.findColour(Label::textColourId);
	Font f = m_stretchinfolabel.getFont();
	for (auto& e : m_param_components)
	{
		
		if (e->m_spec_module_index == selmodule)
		{
			f.setBold(true);
			e->setHighlightColor(Colours::yellow,f);
			e->m_label.setColour(Label::textColourId, Colours::yellow);
		}
		else
		{
			f.setBold(false);
			e->setHighlightColor(defcolor, f);
		}
	}
}

void MainContentComponent::storeSnapShot(int index)
{
	if (m_pars_snapshot.size()<10)
	{
        for (int i=0;i<10;++i)
            m_pars_snapshot.emplace_back("snap"+String(i));
	}
	ValueTree snap("snap");
	for (auto& e : m_param_components)
	{
		snap.setProperty(e->getID(), e->getValue(), nullptr);
	}
	m_pars_snapshot[index] = snap;
}

void MainContentComponent::recallSnapShot(int index)
{
	if (index >= 0 && index < m_pars_snapshot.size())
	{
		ValueTree snap = m_pars_snapshot[index];
		for (auto& e : m_param_components)
		{
			if (e != m_param_components[0])
			{
				double val = snap.getProperty(e->getID());
				e->setValue(val,false);
			}
		}
	}
}

ValueTree MainContentComponent::saveMIDImappings()
{
	ValueTree result("midimappings");
	for (auto& e : m_midimappings)
	{
		ValueTree mappingtree("mapping");
		storeToTreeProperties(mappingtree, nullptr,
			"sourcetype", (int)e.m_src_type,
			"source",e.m_midicc,
			"sourcechan",e.m_midichan,
			"destinationtype",(int)e.m_dest_type,
			"destination",e.m_dest);
		mappingtree.addChild(e.m_map_env->saveState(), -1, nullptr);
		result.addChild(mappingtree,-1,nullptr);
	}
	return result;
}

void MainContentComponent::restoreMIDImappings(ValueTree tree)
{
	if (tree.getNumChildren() > 0)
	{
		std::lock_guard<std::mutex> locker(m_midi_mutex);
		m_midimappings.clear();
		for (auto& e : m_param_components)
			e->setSliderAsMIDILearned(false);
		for (int i = 0; i < tree.getNumChildren(); ++i)
		{
			ValueTree mappingtree = tree.getChild(i);
			int src = 0, srctype = 0, src_chan = 0, dest = 0, dest_type = 0;
			getFromTreeProperties(mappingtree,
				"sourcetype", srctype, "source", src, "sourcechan", src_chan, "destination", dest, "destinationtype", dest_type);
			if ((RemoteSource)srctype == RemoteSource::MIDI_CC)
			{
				RemoteDestination rd = (RemoteDestination)dest_type;
				if (dest >= 0 && dest < m_param_components.size())
					m_param_components[dest]->setSliderAsMIDILearned(true);
				m_midimappings.emplace_back(RemoteSource::MIDI_CC, src_chan, src, rd, dest);
				ValueTree env_tree = mappingtree.getChildWithName("envelope");
				if (env_tree.isValid())
				{
					m_midimappings.back().m_map_env->restoreState(env_tree);
				}
			}
		}
	}
}

ValueTree MainContentComponent::getStateAsTree()
{
	ValueTree state("paulstretch3");
	for (auto& e : m_param_components)
	{
		String id = e->getID();
		double v = e->getValue();
		state.setProperty(id, v, nullptr);
	}
	state.setProperty("loadedfile", m_curfile.getFullPathName(), nullptr);
	storeToTreeProperties(state, nullptr, "timesel", m_wavecomponent.getTimeSelection());
	state.addChild(saveMIDImappings(), -1, nullptr);
	String temp;
	for (auto& e : m_thecontrol->getStretchAudioSource()->getSpectrumProcessOrder())
		temp.append(String(e), 1);
	state.setProperty("spectral_order", temp, nullptr);
	storeToTreeProperties(state, nullptr, "wavezs", m_waveformzs.get_range());
	if (m_param_components[24]->getValue() > 0.5)
		state.setProperty("freezepos", m_thecontrol->getStretchAudioSource()->getInfilePositionPercent(), nullptr);
	return state;
}

void MainContentComponent::restoreStateFromTree(ValueTree state, bool exclude_importedfile)
{
	for (auto& e : m_param_components)
	{
		String id = e->getID();
		double v = e->getValue();
		if (state.hasProperty(id))
		{
			double v2 = state.getProperty(id, v);
			e->setValue(v2, false);
			e->updateComponent();
		}
	}
	if (exclude_importedfile == false)
	{
		String filename = state.getProperty("loadedfile");
		if (filename.isEmpty() == false)
		{
			setAudioFile(File(filename));
		}
	}
	Range<double> timesel;
	getFromTreeProperties(state, "timesel", timesel);
	bool enableloop = m_param_components[25]->getValue() > 0.5;
	m_thecontrol->getStretchAudioSource()->setPlayRange(timesel, enableloop);
	m_wavecomponent.setTimeSelection(timesel);
	String order = state.getProperty("spectral_order", "01234567");

	std::vector<int> temp;
	for (int i = 0; i<order.length(); ++i)
	{
		int index = order[i] - 48;
		if (index >= 0 && index<8)
		{
			temp.push_back(index);
		}
	}
	m_thecontrol->getStretchAudioSource()->setSpectrumProcessOrder(temp);
	m_spec_listbox.setPSControl(m_thecontrol.get());

	ValueTree mappings = state.getChildWithName("midimappings");
	if (mappings.isValid())
	{
		restoreMIDImappings(mappings);
	}
	else Logger::writeToLog("No MIDI mappings tree");
	Range<double> zsstate(0.0, 1.0);
	getFromTreeProperties(state, "wavezs", zsstate);
	m_waveformzs.setRange(zsstate, true);
	MessageManager::callAsync([this]()
	{
		m_thecontrol->ppar.harmonics.enabled = m_param_components[18]->getValue() > 1.0;
		m_thecontrol->update_process_parameters();
	});
}

String MainContentComponent::saveStateAsFile(File f)
{
	ValueTree state = getStateAsTree();
	auto xml = unique_from_raw(state.createXml());
	if (xml != nullptr)
	{
		if (xml->writeToFile(f, String()) == false)
			return "Could not write document file";
	}
	return String();
}

String MainContentComponent::loadStateFromFile(File f)
{
	XmlDocument doc(f);
	auto xml = unique_from_raw(doc.getDocumentElement());
	if (xml != nullptr)
	{
		ValueTree state = ValueTree::fromXml(*xml);
		if (state.isValid() == false)
			return "File did not contain valid ValueTree";
		restoreStateFromTree(state,false);
	}
	else return "File did not contain Xml";
	return String();
}


void MainContentComponent::resized()
{
	m_importfilebut.setBounds(1,1, 100 , 25);
	m_importfilebut.changeWidthToFitText();
	m_recentfilesbut.setBounds(m_importfilebut.getRight() + 1, 1, 100, 25);
	m_recentfilesbut.changeWidthToFitText();
	
	m_playbut.setBounds(m_recentfilesbut.getRight() + 1, 1, 100, 25);
	m_playbut.changeWidthToFitText();
	m_rewindbut.setBounds(m_playbut.getRight() + 1, 1, 100, 25);
	m_rewindbut.changeWidthToFitText();
	m_renderbut.setBounds(m_rewindbut.getRight() + 1, 1, 100, 25);
	m_renderbut.changeWidthToFitText();
    m_menubut.setBounds(m_renderbut.getRight()+1,1,100,25);
    m_menubut.changeWidthToFitText();
	m_progressbar.setBounds(m_menubut.getRight() + 1, 1, getWidth() - m_menubut.getRight() - 2, 25);
    int col_w = getWidth()/2-2;
	int yoffs = m_menubut.getBottom() + 1;
	m_param_components[0]->setBounds(1, yoffs, getWidth() - 2, 48);
	yoffs += 50;
	m_group_stretcher.setBounds(1, yoffs, getWidth() - 2, 100);
	yoffs += 20;
	m_param_components[1]->setBounds(1, yoffs, getWidth()-5, 24); // strech amount
	yoffs += 25;
	m_param_components[2]->setBounds(1, yoffs, getWidth()-5, 24); // fft size
	yoffs += 25;
	col_w = getWidth() / 10 - 2;
	m_param_components[3]->setBounds(1, yoffs, 3*col_w, 24); // onset detect
	m_param_components[23]->setBounds(m_param_components[3]->getRight()+1, yoffs, 2*col_w-5, 24); // fft window type
	m_param_components[22]->setBounds(m_param_components[23]->getRight()+1, yoffs, 3 * col_w - 5, 24); // loop xfade len
	m_param_components[24]->setBounds(m_param_components[22]->getRight() + 1, yoffs, 1*col_w, 24);
	
	m_param_components[25]->setBounds(m_param_components[24]->getRight() + 1, yoffs, 1*col_w, 24);
	

	yoffs = m_group_stretcher.getBottom() + 1;
	col_w = getWidth() / 2 - 2;
	m_group_pitch.setBounds(1, yoffs, getWidth(), 120);
	yoffs += 10;
	m_param_components[4]->setBounds(1, yoffs, getWidth()-2, 24);
	yoffs += 25;
	m_param_components[6]->setBounds(1, yoffs, col_w, 24);
	m_param_components[7]->setBounds(col_w+1, yoffs, col_w, 24);
	yoffs += 25;
	m_param_components[8]->setBounds(1, yoffs, col_w, 24);
	m_param_components[9]->setBounds(col_w + 1, yoffs, col_w, 24);
	yoffs += 25;
	m_param_components[10]->setBounds(1, yoffs, col_w, 24);
	m_param_components[11]->setBounds(col_w + 1, yoffs, col_w, 24);
	
	yoffs = m_group_pitch.getBottom() + 1;
	m_group_harmonics.setBounds(1, yoffs, getWidth() - 1, 65);
	yoffs += 10;
	m_param_components[18]->setBounds(1, yoffs, col_w, 24);
	m_param_components[19]->setBounds(col_w+1, yoffs, col_w, 24);
	yoffs += 25;
	m_param_components[20]->setBounds(1, yoffs, col_w, 24);
	m_param_components[21]->setBounds(col_w + 1, yoffs, col_w, 24);
	yoffs = m_group_harmonics.getBottom() + 1;
	m_group_tonalvsnoise.setBounds(1, yoffs, col_w, 65);
	yoffs += 10;
	m_param_components[14]->setBounds(1, yoffs, col_w, 24);
	yoffs += 25;
	m_param_components[15]->setBounds(1, yoffs, col_w, 24);
	yoffs = m_group_harmonics.getBottom() + 1;
	m_group_filter.setBounds(m_group_tonalvsnoise.getRight() + 1, yoffs, col_w, 65);
	yoffs += 10;
	m_param_components[16]->setBounds(m_group_tonalvsnoise.getRight()+1, yoffs, col_w, 24);
	yoffs += 25;
	m_param_components[17]->setBounds(m_group_tonalvsnoise.getRight()+1, yoffs, col_w, 24);
	yoffs = m_group_filter.getBottom() + 1;
	// 12 compress, 13 spread
	
	col_w = getWidth() / 3 - 2;
	m_group_compressor.setBounds(1, yoffs, col_w, 40);
	yoffs += 10;
	m_param_components[12]->setBounds(1, yoffs, col_w, 24);
	m_group_spread.setBounds(m_group_compressor.getRight() + 1, m_group_compressor.getY(), col_w, 40);
	m_param_components[13]->setBounds(col_w+1, yoffs, col_w, 24);
	yoffs = m_group_compressor.getBottom() + 1;
	m_group_freqshift.setBounds(col_w * 2 + 1, m_group_compressor.getY(), col_w, 40);
	m_param_components[5]->setBounds(col_w * 2 + 1, m_group_compressor.getY()+10, col_w, 24);
    int pcompw = 200;
	m_stretchinfolabel.setBounds(1, getHeight() - 25, getWidth()-100, 24);
	m_perfcomponent.setBounds(getWidth() - pcompw -1, getHeight() - 25, pcompw, 24);
	int ycor = yoffs + 1;
	int speclistw = 130;
	m_group_specorder.setBounds(1, ycor, speclistw, 22 * 8 + 15 + 40);
	m_spec_listbox.setBounds(6, ycor+20, speclistw-12, 22*8);
	
	m_move_spec_up.setBounds(6, m_spec_listbox.getBottom() + 1, 60, 25);
	m_move_spec_down.setBounds(m_move_spec_up.getRight()+1, m_spec_listbox.getBottom() + 1, 60, 25);
	int wavewidth = 0;
	if (m_aviscomp.isVisible())
		wavewidth = getWidth() - 102 -speclistw;
	else wavewidth = getWidth() - 2 -speclistw;
	m_wavecomponent.setBounds(m_group_specorder.getRight()+1, ycor, wavewidth, getHeight()-ycor-40);
	m_aviscomp.setBounds(m_wavecomponent.getRight()+2, ycor+20, 98, getHeight() - ycor - 60);
	m_waveformzs.setBounds(m_wavecomponent.getX(), m_wavecomponent.getBottom(), wavewidth, 15);
#ifdef USE_LUA_SCRIPTING
	try
    {
        m_lua_gui["on_resize"](getWidth(),getHeight());
    }
    catch (std::exception& excep)
    {
        Logger::writeToLog(excep.what());
    }
#endif
}

void MainContentComponent::sliderValueChanged(Slider * /*slid*/)
{
	//Logger::writeToLog("val changed " + String(slid->getValue()));
	m_thecontrol->update_process_parameters();
}

void MainContentComponent::setAudioFile(File file)
{
	m_importfilebut.setEnabled(false);
	m_recentfilesbut.setEnabled(false);
	m_stretchinfolabel.setText("Importing " + file.getFullPathName(),dontSendNotification);
	m_thecontrol->set_input_file(file,[file,this](String r)
    {
		m_importfilebut.setEnabled(true);
		m_recentfilesbut.setEnabled(true);
		if (r.isEmpty()==false)
        {
            showBubbleMessage(&m_wavecomponent, r, 5000, true);
            return;
        }
        if (m_thecontrol->get_input_filename().isEmpty() == false)
            m_playbut.setEnabled(true);
        m_wavecomponent.setAudioFile(file);
        m_curfile = file;
        m_recent_audio_files.addFile(file);
        updateInfoLabel();
    });
}

void MainContentComponent::startPlay()
{
	m_stoprequested = false;
	auto timesel = m_wavecomponent.getTimeSelection();
    if (m_record_output == true)
    {
        if (m_liverecordfile != File())
        {
            m_thecontrol->setOutputAudioFileToRecord(m_liverecordfile);
        }
    }
    
	String err;
	m_thecontrol->startplay(false, true, timesel, m_num_outchans,err);
	if (err.isEmpty() == false)
		showBubbleMessage(&m_playbut, err, 5000, true);
	m_playbut.setButtonText("Stop");
    //m_param_sliders[2]->setEnabled(false);
    m_wavecomponent.setTimerEnabled(true);
    m_aviscomp.setRepaintRate(10);
}

void MainContentComponent::stopPlay()
{
    m_thecontrol->stopplay();
    m_playbut.setButtonText("Play");
    m_param_components[2]->setEnabled(true);
    m_wavecomponent.setTimerEnabled(false);
    m_aviscomp.setRepaintRate(1);
	
}

void MainContentComponent::handleIncomingMidiMessage(MidiInput * /*source*/, const MidiMessage & message)
{
	std::lock_guard<std::mutex> locker(m_midi_mutex);
	if (message.isController())
	{
		if (m_param_to_learn >= 0)
		{
			m_midimappings.emplace_back(RemoteSource::MIDI_CC, message.getChannel(),
				message.getControllerNumber(), RemoteDestination::Parameter, m_param_to_learn);
			//Logger::writeToLog("learned " + String(m_param_to_learn));
			int parid = m_param_to_learn;
			m_param_to_learn = -1;
			MessageManager::callAsync([parid,this]() 
			{ 
				m_param_components[parid]->setSliderAsMIDILearned(true);
				updateInfoLabel(); 
			});
			return;
		}
#ifdef USE_LUA_SCRIPTING
		if (m_lua_midi_enabled == true)
		{
			try
			{
				m_lua_midi["On_MIDI_CC"](message.getChannel(), message.getControllerNumber(), message.getControllerValue());
				return;
			}
			catch (const std::exception&)
			{
				// Amazing exception handling here...
				return;
			}
			
		} 
#endif
      
		for (auto& e : m_midimappings)
		{
			if (e.m_midicc == message.getControllerNumber() && e.m_midichan == message.getChannel())
			{
				double val = 1.0 / 127 * message.getControllerValue();
				val = e.m_map_env->GetInterpolatedNodeValue(val);
				int slidindex = e.m_dest;
				if (slidindex >= 0 && slidindex < m_param_components.size())
				{
					if (slidindex != 2)
                        m_param_components[slidindex]->setValue(val,true); // fft size would need special handling
                    
				}
			}
		}
	}
}

void MainContentComponent::saveDocument()
{
	if (m_cur_doc_file != File())
		saveStateAsFile(m_cur_doc_file);
}

void MainContentComponent::saveDocumentAs()
{
	String lastsavefolder = g_propsfile->getValue("last_folder_save_main_document");
	FileChooser myChooser("Save as...",
		File(lastsavefolder),
		"*"+g_pf_ext);
	if (myChooser.browseForFileToSave(true))
	{
		if (saveStateAsFile(myChooser.getResult()).isEmpty())
		{
			m_cur_doc_file = myChooser.getResult();
			m_recent_doc_files.addFile(myChooser.getResult());
		}
        grabKeyboardFocus();
        g_propsfile->setValue("last_folder_save_main_document", myChooser.getResult().getParentDirectory().getFullPathName());
	}
    getParentComponent()->toFront(true);
}

void MainContentComponent::openDocument()
{
	String lastsavefolder = g_propsfile->getValue("last_folder_save_main_document");
    {
        FileChooser myChooser("Open...",
            File(lastsavefolder),
            "*"+g_pf_ext);
        if (myChooser.browseForFileToOpen())
        {
			if (loadStateFromFile(myChooser.getResult()).isEmpty())
			{
				m_cur_doc_file = myChooser.getResult();
				m_recent_doc_files.addFile(myChooser.getResult());
			}
            
            g_propsfile->setValue("last_folder_save_main_document", myChooser.getResult().getParentDirectory().getFullPathName());
        }
    }
    getParentComponent()->toFront(true);
}

void MainContentComponent::loadLuaMIDIScript(File f)
{
#ifdef USE_LUA_SCRIPTING
	std::lock_guard<std::mutex> locker(m_midi_mutex);
	try
	{
		m_lua_midi.script_file(f.getFullPathName().toStdString());
    }
	catch (const std::exception& excep)
	{
		m_stretchinfolabel.setText(CharPointer_UTF8(excep.what()), dontSendNotification);
	}
#endif
}

void MainContentComponent::loadLuaGUIScript(File f)
{
#ifdef USE_LUA_SCRIPTING
	try
	{
		m_lua_gui = sol::state();
		m_lua_gui.open_libraries();
		m_lua_gui["set_bounds"] = [this](const char* id, int x, int y, int w, int h)
		{
			for (auto& e : getChildren())
			{
				if (e->getName() == id)
				{
					e->setBounds(x, y, w, h);
					return;
				}
			}
			Logger::writeToLog("No child with name " + String(id));
		};
		m_lua_gui.script_file(f.getFullPathName().toStdString());
		m_lua_gui["on_gui_init"]();
		resized();
	}
	catch (const std::exception& excep)
	{
		m_stretchinfolabel.setText(CharPointer_UTF8(excep.what()), dontSendNotification);
	}
#endif
}

void MainContentComponent::renderToFile(File renderfile,int64_t numLoops, 
	int wavformat, int sampleRate, bool clipfloats, double maxdur)
{
	File outFile(renderfile);
	if (outFile == m_curfile)
	{
		m_stretchinfolabel.setText("Cannot render overwriting the source file!", dontSendNotification);
		Timer::callAfterDelay(5000, [this]() { updateInfoLabel(); });
		return;
	}
	g_propsfile->setValue("last_folder_export_file", outFile.getParentDirectory().getFullPathName());
	m_renderbut.setButtonText("Cancel");
	m_progressbar.setVisible(true);
	auto timesel = m_wavecomponent.getTimeSelection();
	RenderParameters rpars;
	rpars.inaudio = m_curfile;
	rpars.outaudio = outFile;
	rpars.pos1 = timesel.getStart();
	rpars.pos2 = timesel.getEnd();
	rpars.numLoops = numLoops;
	rpars.sampleRate = sampleRate;
	rpars.numoutchans = m_num_outchans;
	rpars.minrenderlen = m_min_render_len;
	rpars.maxrenderlen = maxdur;
	rpars.voldb = m_param_components[0]->getValue();
	rpars.wavformat = jlimit(0,2,wavformat);
	rpars.clipFloatOutput = clipfloats;
	rpars.completion_callback = [this](RenderInfoRef result)
	{
		m_stretchinfolabel.setText(result->m_txt, dontSendNotification);
		m_renderbut.setButtonText("Render...");
		m_renderinfo = nullptr;
		m_render_progress = 0.0;
		m_progressbar.setVisible(false);
		toFront(true);
		Timer::callAfterDelay(2000, [this]() { updateInfoLabel(); });
	};
	m_renderinfo = m_thecontrol->Render2(rpars);
}

void MainContentComponent::showRenderWindow()
{
	if (m_renderinfo != nullptr)
	{
		m_renderinfo->m_cancel = true;
		return;
	}
	if (m_curfile.existsAsFile() == false)
		return;

	RenderSettingsComponent* rcomp = new RenderSettingsComponent(this);
	rcomp->setVisible(true);
	rcomp->setSize(700, rcomp->getPreferredHeight());
	/*int dlgresult = */ DialogWindow::showModalDialog("Render", rcomp, this, Colours::darkgrey, true);
	delete rcomp;
}

void MainContentComponent::timerCallback(int id)
{
	if (id == 1)
	{
		if (m_renderinfo != nullptr)
			m_render_progress = m_renderinfo->m_progress_percent;
		if (m_stoprequested == false && m_thecontrol->playing() && m_thecontrol->getStretchAudioSource() && m_thecontrol->getStretchAudioSource()->hasReachedEnd())
		{
			m_stoprequested = true;
			double prebufseconds = m_thecontrol->getPreBufferAmountSeconds();
			Timer::callAfterDelay((int)(prebufseconds * 1000), [this]()
			{
				//Logger::writeToLog("Calling stop play on GUI thread with a delay "+ String((int64_t)Thread::getCurrentThreadId()));
				stopPlay();
			});
		}
			
	}
	if (id == 2)
	{
		for (auto& e : m_param_components)
			e->updateComponent();
	}
}

void MainContentComponent::updateInfoLabel()
{
	String txt = String(m_thecontrol->get_stretch_info(m_wavecomponent.getTimeSelection()));
	if (m_thecontrol->getStretchAudioSource()->isResampling())
        txt+=" SRC";
	MessageManager::callAsync([this,txt]()
	{
		m_stretchinfolabel.setText(txt, dontSendNotification);
	});
	
}
