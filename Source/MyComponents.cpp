#include "MyComponents.h"

extern std::unique_ptr<AudioFormatManager> g_audioformatmanager;
extern std::unique_ptr<PropertiesFile> g_propsfile;

PerfMeterComponent::PerfMeterComponent()
{
	setTooltip("CPU RT tells how much CPU is spent in the high priority audio processing. This should remain zero or very low on modern computers.\n"
		"PREBUF tells how much prebuffered audio is available. Increase prebuffering amount in the settings if this tends to not stay filled.");
}

void PerfMeterComponent::paint(juce::Graphics &g)
{
	if (m_control == nullptr)
		return;
	g.setColour(Colours::white);
	g.setFont(10.0f);
	g.drawText("CPU RT", 1, 1, 40, 10, Justification::centredLeft);
	g.drawText("PREBUF", 1, 11, 40, 10, Justification::centredLeft);
	g.setColour(Colours::lightgreen);
	double availw = getWidth() - 41;
	g.fillRect(41, 1, (int)(availw*m_control->m_adm->getCpuUsage()), 9);
	g.fillRect(41, 11, (int)(availw*m_control->getPreBufferingPercent()), 9);
}

void PerfMeterComponent::setPSControl(Control *c)
{
	m_control = c;
	startTimerHz(20);
}

void PerfMeterComponent::timerCallback()
{
	repaint();
}

SpectralProcessesListBox::SpectralProcessesListBox()
{
	m_spec_list = make_spectrum_processes();
	for (auto& e : m_spec_list)
		m_spec_order.push_back(e.m_index);
	setModel(this);
}

void SpectralProcessesListBox::setPSControl(Control * c)
{
	m_PScontrol = c;
	m_spec_order = m_PScontrol->getStretchAudioSource()->getSpectrumProcessOrder();
	updateContent();
	repaint();
}

template<typename I>
inline std::pair<I, I> slide(I f, I l, I p)
{
	if (p < f) return { p, std::rotate(p,f,l) };
	if (l < p) return { std::rotate(f,l,p), p };
	return { f, l };
}

void SpectralProcessesListBox::selectedRowsChanged(int row)
{
	if (RowClickedCallBack && row >= 0 && row<m_spec_order.size())
		RowClickedCallBack(m_spec_order[row]);
}

int SpectralProcessesListBox::getNumRows()
{
	return (int)m_spec_list.size();
}

void SpectralProcessesListBox::paintListBoxItem(int rowNumber, Graphics & g, int width, int height, bool rowIsSelected)
{
	if (rowNumber < 0 || rowNumber >= m_spec_list.size())
		return;
	if (rowIsSelected == true)
		g.fillAll(Colours::blue);
	else g.fillAll(Colours::grey);
	g.setColour(Colours::white);
	g.setFont(height*0.8f);
	int index = m_spec_order[rowNumber];
	g.drawText(m_spec_list[index].m_name, 5, 0, width, height, Justification::centredLeft);

}

void SpectralProcessesListBox::moveSelectedUpOrDown(bool up)
{
	int cur_row = getSelectedRow(0);
	int old_row = cur_row;
	if (cur_row >= 0 && cur_row < m_spec_order.size())
	{
		if (up)
			--cur_row;
		else ++cur_row;
		if (cur_row < 0)
			cur_row = (int)m_spec_order.size() - 1;
		if (cur_row >= (int)m_spec_order.size())
			cur_row = 0;
		std::swap(m_spec_order[old_row], m_spec_order[cur_row]);
		if (m_PScontrol)
			m_PScontrol->getStretchAudioSource()->setSpectrumProcessOrder(m_spec_order);
		selectRow(cur_row);
		updateContent();
		repaint();
	}
}

void zoom_scrollbar::mouseDown(const MouseEvent &e)
{
	m_drag_start_x = e.x;
}

void zoom_scrollbar::mouseMove(const MouseEvent &e)
{
	auto ha = get_hot_area(e.x, e.y);
	if (ha == ha_left_edge || m_hot_area == ha_right_edge)
		setMouseCursor(MouseCursor::LeftRightResizeCursor);
	else
		setMouseCursor(MouseCursor::NormalCursor);
	if (ha != m_hot_area)
	{
		m_hot_area = ha;
		repaint();
	}
}

void zoom_scrollbar::mouseDrag(const MouseEvent &e)
{
	if (m_hot_area == ha_none)
		return;
	if (m_hot_area == ha_left_edge)
	{
		double new_left_edge = 1.0 / getWidth()*e.x;
		m_therange.setStart(jlimit(0.0, m_therange.getEnd() - 0.01, new_left_edge));
		repaint();
	}
	if (m_hot_area == ha_right_edge)
	{
		double new_right_edge = 1.0 / getWidth()*e.x;
		m_therange.setEnd(jlimit(m_therange.getStart() + 0.01, 1.0, new_right_edge));
		repaint();
	}
	if (m_hot_area == ha_handle)
	{
		double delta = 1.0 / getWidth()*(e.x - m_drag_start_x);
		//double old_start = m_start;
		//double old_end = m_end;
		double old_len = m_therange.getLength();
		m_therange.setStart(jlimit(0.0, 1.0 - old_len, m_therange.getStart() + delta));
		m_therange.setEnd(jlimit(old_len, m_therange.getStart() + old_len, m_therange.getEnd() + delta));
		m_drag_start_x = e.x;
		repaint();
	}
	if (RangeChanged)
		RangeChanged(m_therange);
}

void zoom_scrollbar::mouseEnter(const MouseEvent & event)
{
	m_hot_area = get_hot_area(event.x, event.y);
	repaint();
}

void zoom_scrollbar::mouseExit(const MouseEvent &)
{
	m_hot_area = ha_none;
	repaint();
}

void zoom_scrollbar::paint(Graphics &g)
{
	g.setColour(Colours::darkgrey);
	g.fillRect(0, 0, getWidth(), getHeight());
	int x0 = (int)(getWidth()*m_therange.getStart());
	int x1 = (int)(getWidth()*m_therange.getEnd());
	if (m_hot_area != ha_none)
		g.setColour(Colours::white);
	else g.setColour(Colours::lightgrey);
	g.fillRect(x0, 0, x1 - x0, getHeight());
}

void zoom_scrollbar::setRange(Range<double> rng, bool docallback)
{
	m_therange = rng.constrainRange({ 0.0,1.0 });
	if (RangeChanged && docallback)
		RangeChanged(m_therange);
	repaint();
}

zoom_scrollbar::hot_area zoom_scrollbar::get_hot_area(int x, int)
{
	int x0 = (int)(getWidth()*m_therange.getStart());
	int x1 = (int)(getWidth()*m_therange.getEnd());
	if (is_in_range(x, x0 - 5, x0 + 5))
		return ha_left_edge;
	if (is_in_range(x, x1 - 5, x1 + 5))
		return ha_right_edge;
	if (is_in_range(x, x0 + 5, x1 - 5))
		return ha_handle;
	return ha_none;
}

WaveformComponent::WaveformComponent() : m_thumbcache(100)
{
    if (m_use_opengl == true)
		m_ogl.attachTo(*this);
    // The default priority of 2 is a bit too low in some cases, it seems...
	m_thumbcache.getTimeSliceThread().setPriority(3);
	m_thumb = std::make_unique<AudioThumbnail>(512, *g_audioformatmanager, m_thumbcache);
	m_thumb->addChangeListener(this);
	setOpaque(true);
}

WaveformComponent::~WaveformComponent()
{
	if (m_use_opengl == true)
		m_ogl.detach();
}

void WaveformComponent::changeListenerCallback(ChangeBroadcaster * /*cb*/)
{
	m_waveimage = Image();
	repaint();
}

void WaveformComponent::paint(Graphics & g)
{
	//Logger::writeToLog("Waveform component paint");
	g.fillAll(Colours::black);
	g.setColour(Colours::darkgrey);
	g.fillRect(0, 0, getWidth(), m_topmargin);
	if (m_thumb == nullptr || m_thumb->getTotalLength() < 0.1)
	{
		g.setColour(Colours::aqua.darker());
		g.drawText("No file loaded", 2, m_topmargin + 2, getWidth(), 20, Justification::topLeft);
		return;
	}
	g.setColour(Colours::lightslategrey);
	double thumblen = m_thumb->getTotalLength();
	double tick_interval = 1.0;
	if (thumblen > 60.0)
		tick_interval = 5.0;
	for (double secs = 0.0; secs < thumblen; secs += tick_interval)
	{
		float tickxcor = (float)jmap<double>(secs,
			thumblen*m_view_range.getStart(), thumblen*m_view_range.getEnd(), 0.0f, (float)getWidth());
		g.drawLine(tickxcor, 0.0, tickxcor, (float)m_topmargin, 1.0f);
	}
    bool m_use_cached_image = true;
    if (m_use_cached_image==true)
    {
		if (m_waveimage.isValid() == false || m_waveimage.getWidth() != getWidth()
			|| m_waveimage.getHeight() != getHeight() - m_topmargin)
		{
			//Logger::writeToLog("updating cached waveform image");
			m_waveimage = Image(Image::ARGB, getWidth(), getHeight() - m_topmargin, true);
			Graphics tempg(m_waveimage);
			tempg.fillAll(Colours::black);
			tempg.setColour(Colours::darkgrey);
			m_thumb->drawChannels(tempg, { 0,0,getWidth(),getHeight() - m_topmargin },
				thumblen*m_view_range.getStart(), thumblen*m_view_range.getEnd(), 1.0f);
		}
        g.drawImage(m_waveimage, 0, m_topmargin, getWidth(), getHeight() - m_topmargin, 0, 0, getWidth(), getHeight() - m_topmargin);
        
    } else
    {
        //g.fillAll(Colours::black);
        g.setColour(Colours::darkgrey);
        m_thumb->drawChannels(g, { 0,m_topmargin,getWidth(),getHeight() - m_topmargin },
                              thumblen*m_view_range.getStart(), thumblen*m_view_range.getEnd(), 1.0f);
    }
    
    //g.setColour(Colours::darkgrey);
	//m_thumb->drawChannels(g, { 0,m_topmargin,getWidth(),getHeight()-m_topmargin }, 
	//	0.0, thumblen, 1.0f);
	g.setColour(Colours::white.withAlpha(0.5f));
	int xcorleft = (int)jmap<double>(m_time_sel_start, m_view_range.getStart(), m_view_range.getEnd(), 0, getWidth());
	int xcorright = (int)jmap<double>(m_time_sel_end, m_view_range.getStart(), m_view_range.getEnd(), 0, getWidth());
	g.fillRect(xcorleft, m_topmargin, xcorright - xcorleft, getHeight() - m_topmargin);
	if (m_file_cached.first.getLength() > 0.0 &&
		(bool)ShowFileCacheRange.getValue())
	{
		g.setColour(Colours::red.withAlpha(0.2f));
		xcorleft = (int)jmap<double>(m_file_cached.first.getStart(), m_view_range.getStart(), m_view_range.getEnd(), 0, getWidth());
		xcorright = (int)jmap<double>(m_file_cached.first.getEnd(), m_view_range.getStart(), m_view_range.getEnd(), 0, getWidth());
		g.fillRect(xcorleft, 0, xcorright - xcorleft, m_topmargin / 2);
		xcorleft = (int)jmap<double>(m_file_cached.second.getStart(), m_view_range.getStart(), m_view_range.getEnd(), 0, getWidth());
		xcorright = (int)jmap<double>(m_file_cached.second.getEnd(), m_view_range.getStart(), m_view_range.getEnd(), 0, getWidth());
		if (xcorright - xcorleft>0)
		{
			g.setColour(Colours::blue.withAlpha(0.2f));
			g.fillRect(xcorleft, m_topmargin / 2, xcorright - xcorleft, m_topmargin / 2);
		}
	}

	g.setColour(Colours::white);
	if (CursorPosCallback)
	{
		double pos = jmap<double>(CursorPosCallback(), m_view_range.getStart(), m_view_range.getEnd(), 0, getWidth());
		g.fillRect((int)pos, m_topmargin, 1, getHeight() - m_topmargin);
	}
	g.setColour(Colours::aqua.darker());
	g.drawText(m_curfile.getFullPathName(), 2, m_topmargin + 2, getWidth(), 20, Justification::topLeft);
}

void WaveformComponent::setAudioFile(File f)
{
	if (f.existsAsFile())
	{
		m_waveimage = Image();
		if (m_thumb != nullptr && f == m_curfile) // reloading same file, might happen that the overview needs to be redone...
			m_thumbcache.removeThumb(m_thumb->getHashCode());
		m_thumb->setSource(new FileInputSource(f));
		m_curfile = f;
	}
	else
	{
		m_thumb->setSource(nullptr);
	}

}

void WaveformComponent::timerCallback()
{
	repaint();
}

void WaveformComponent::setFileCachedRange(std::pair<Range<double>, Range<double>> rng)
{
	m_file_cached = rng;
	//repaint();
}

void WaveformComponent::setTimerEnabled(bool b)
{
	if (b == true)
		startTimer(100);
	else
		stopTimer();
}

void WaveformComponent::setViewRange(Range<double> rng)
{
	m_view_range = rng;
	m_waveimage = Image();
	repaint();
}


void WaveformComponent::mouseDown(const MouseEvent & e)
{
	m_mousedown = true;
	double pos = jmap<double>(e.x, 0, getWidth(), m_view_range.getStart(), m_view_range.getEnd());
	if (e.y < m_topmargin)
	{
		if (SeekCallback)
			SeekCallback(pos);
		m_didseek = true;
	}
	else
	{
		m_time_sel_drag_target = getTimeSelectionEdge(e.x,e.y);
		m_drag_time_start = pos;
		if (m_time_sel_drag_target == 0)
		{
			m_time_sel_start = -1.0;
			m_time_sel_end = -1.0;
		}
	}

	repaint();
}

void WaveformComponent::mouseUp(const MouseEvent & /*e*/)
{
	m_mousedown = false;
	m_didseek = false;
	if (m_didchangetimeselection)
	{
		TimeSelectionChangedCallback(Range<double>(m_time_sel_start, m_time_sel_end), 1);
		m_didchangetimeselection = false;
	}
}

void WaveformComponent::mouseDrag(const MouseEvent & e)
{
	if (m_didseek == true)
		return;
	if (m_time_sel_drag_target == 0)
	{
		m_time_sel_start = m_drag_time_start;
		m_time_sel_end = jmap<double>(e.x, 0, getWidth(), m_view_range.getStart(), m_view_range.getEnd());
	}
	if (m_time_sel_drag_target == 1)
	{
		m_time_sel_start = jmap<double>(e.x, 0, getWidth(), m_view_range.getStart(), m_view_range.getEnd());
	}
	if (m_time_sel_drag_target == 2)
	{
		m_time_sel_end = jmap<double>(e.x, 0, getWidth(), m_view_range.getStart(), m_view_range.getEnd());
	}
	if (m_time_sel_start > m_time_sel_end)
	{
		std::swap(m_time_sel_start, m_time_sel_end);
		if (m_time_sel_drag_target == 1)
			m_time_sel_drag_target = 2;
		else if (m_time_sel_drag_target == 2)
			m_time_sel_drag_target = 1;
	}
	m_time_sel_start = jlimit(0.0, 1.0, m_time_sel_start);
	m_time_sel_end = jlimit(0.0, 1.0, m_time_sel_end);
	
	if (TimeSelectionChangedCallback)
	{
		if (m_time_sel_end>m_time_sel_start)
			TimeSelectionChangedCallback(Range<double>(m_time_sel_start, m_time_sel_end), 0);
		else
			TimeSelectionChangedCallback(Range<double>(0.0, 1.0), 0);
	}
	m_didchangetimeselection = true;
	repaint();
}

void WaveformComponent::mouseMove(const MouseEvent & e)
{
	m_time_sel_drag_target = getTimeSelectionEdge(e.x, e.y);
	if (m_time_sel_drag_target == 0)
		setMouseCursor(MouseCursor::NormalCursor);
	if (m_time_sel_drag_target == 1)
		setMouseCursor(MouseCursor::LeftRightResizeCursor);
	if (m_time_sel_drag_target == 2)
		setMouseCursor(MouseCursor::LeftRightResizeCursor);
	
}

int WaveformComponent::getTimeSelectionEdge(int x, int y)
{
	int xcorleft = (int)jmap<double>(m_time_sel_start, m_view_range.getStart(), m_view_range.getEnd(), 0, getWidth());
	int xcorright = (int)jmap<double>(m_time_sel_end, m_view_range.getStart(), m_view_range.getEnd(), 0, getWidth());
	if (Rectangle<int>(xcorleft - 5, m_topmargin, 10, getHeight() - m_topmargin).contains(x, y))
		return 1;
	if (Rectangle<int>(xcorright - 5, m_topmargin, 10, getHeight() - m_topmargin).contains(x, y))
		return 2;
	return 0;
}

ParamComponent::ParamComponent(String id,String name, double minval, double maxval, double defval, double step, bool notifyOnMouseUp)
{
    setName(id);
    m_id = id;
	m_notif_on_up = notifyOnMouseUp;
	addAndMakeVisible(&m_label);
	m_label.setText(name, dontSendNotification);
	m_slider = std::make_unique<ContextMenuComponent<Slider>>();
	m_origslidercolor = m_slider->findColour(Slider::thumbColourId);
	addAndMakeVisible(m_slider.get());
	m_val = defval;
	m_slider->setRange(minval, maxval, step);
	m_slider->setDoubleClickReturnValue(true, defval);
	m_slider->setValue(defval,dontSendNotification);
	m_slider->addListener(this);
}

ParamComponent::ParamComponent(String id, String name, Value & val, double minval, double maxval, double defval, double step) :
	ParamComponent(id, name, minval, maxval, defval, step)
{
	val.referTo(m_slider->getValueObject());
}

ParamComponent::ParamComponent(String id, String name, bool defval)
{
	setName(id);
    m_id = id;
	m_label.setText(name, dontSendNotification);
	m_togglebut = std::make_unique<ContextMenuComponent<ToggleButton>>();
	m_togglebut->setButtonText(name);
	m_togglebut->setToggleState(defval, dontSendNotification);
	m_togglebut->addListener(this);
	m_val = defval;
	addAndMakeVisible(m_togglebut.get());
}

ParamComponent::ParamComponent(String id, String name, StringArray choices, int defaultchoice)
{
	setName(id);
    m_id = id;
	m_label.setText(name, dontSendNotification);
	m_combobox = std::make_unique<ContextMenuComponent<ComboBox>>();
	addAndMakeVisible(&m_label);
	addAndMakeVisible(m_combobox.get());
	m_val = defaultchoice;
	for (int i = 0; i<choices.size(); ++i)
		m_combobox->addItem(choices[i], i + 1);
	m_combobox->setSelectedId(defaultchoice + 1,dontSendNotification);
	m_combobox->addListener(this);
}

void ParamComponent::buttonClicked(Button * but)
{
	if (but->getToggleState() == true)
		m_val = 1.0;
	else m_val = 0.0;
	if (ValueChangedCallback)
		ValueChangedCallback(but->getToggleState());
	if (AdditionalCallBack)
		AdditionalCallBack();
}

void ParamComponent::comboBoxChanged(ComboBox * cb)
{
	m_val = cb->getSelectedId() - 1;
	if (ValueChangedCallback)
		ValueChangedCallback(cb->getSelectedId() - 1);
	if (AdditionalCallBack)
		AdditionalCallBack();
}

void ParamComponent::setHighlightColor(Colour c, Font f)
{
	if (m_togglebut != nullptr)
	{
		m_togglebut->setColour(ToggleButton::textColourId, c);
	}
	else
	{
		m_label.setColour(Label::textColourId, c);
		m_label.setFont(f);
	}
}

void ParamComponent::resized()
{
	m_label.setBounds(1, 1, 150, getHeight());
	if (m_slider != nullptr)
	{
		m_slider->setBounds(m_label.getRight() + 1, 1, getWidth() - m_label.getWidth() - 2, getHeight());
	}
	if (m_togglebut != nullptr)
		m_togglebut->setBounds(1, 1, getWidth() - 2, getHeight());
	if (m_combobox != nullptr)
		m_combobox->setBounds(m_label.getRight() + 1, 1, getWidth() - m_label.getWidth() - 2, getHeight() - 2);
}

void ParamComponent::sliderValueChanged(Slider * slid)
{
	m_val = slid->getValue();
	if (m_notif_on_up)
		return;
	setValue(slid->getValue(), false);
}

void ParamComponent::sliderDragEnded(Slider * slid)
{
	if (m_notif_on_up == false)
		return;
	setValue(slid->getValue(), false);
}

void ParamComponent::setValue(double x, bool isnormalized)
{
	m_val = x;
	if (m_slider != nullptr && isnormalized)
		m_val = m_slider->proportionOfLengthToValue(x);
	if (m_combobox != nullptr && isnormalized)
		m_val = x * (m_combobox->getNumItems() - 1);
	if (ValueChangedCallback)
		ValueChangedCallback(m_val);
	if (AdditionalCallBack)
		AdditionalCallBack();
}

double ParamComponent::getValueFromNormalized(double x)
{
	if (m_slider == nullptr)
		return 0.0;
	return m_slider->proportionOfLengthToValue(x);
}

void ParamComponent::updateComponent()
{
	if (m_slider != nullptr && m_slider->getValue() != m_val)
		m_slider->setValue(m_val, dontSendNotification);
	if (m_combobox != nullptr && (m_combobox->getSelectedId() - 1) != (int)m_val)
		m_combobox->setSelectedId((int)m_val + 1, dontSendNotification);
	if (m_togglebut != nullptr && m_togglebut->getToggleState() != (bool)m_val)
		m_togglebut->setToggleState((bool)m_val, dontSendNotification);
}

void ParamComponent::setSliderAsMIDILearned(bool b)
{
	if (m_slider == nullptr)
		return;
	if (b == false)
		m_slider->setColour(Slider::thumbColourId, m_origslidercolor);
	else m_slider->setColour(Slider::thumbColourId, Colours::lightyellow);
}

void ParamComponent::setPopUpMenuCallback(std::function<void(Component*)> cb)
{
	if (m_slider != nullptr)
		m_slider->PopMenuCallback = cb;
	if (m_togglebut != nullptr)
		m_togglebut->PopMenuCallback = cb;
	if (m_combobox != nullptr)
		m_combobox->PopMenuCallback = cb;
}
